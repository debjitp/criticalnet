#===============================================================================
#
#         FILE: visualize.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 31-01-2020
#     REVISION: 
#    LMODIFIED: Sat 15 Feb 2020 02:59:09 AM EST
#===============================================================================

import seaborn as sns
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from argparse import ArgumentParser
import pprint as pp
from itertools import combinations
#### for cross verification purposes ####
#from shapely.geometry import Polygon as Poly
import numpy as np
#### for cross verification purposes ####

def visualize(cloc_, design, phase):
    
    cfile = open(cloc_ + '/' + design + '_verilog_netlist_' + phase + '.loc', 'r')
    clines = cfile.readlines()
    cfile.close()

    Loc_Map = {}
    
    Max_X_loc = -1
    Max_Y_loc = -1

    for line in clines:
        line_ = line.split()
        X_loc = line_[1]
        Y_loc = line_[2]
        Max_X_loc = max(Max_X_loc, int(X_loc))
        Max_Y_loc = max(Max_Y_loc, int(Y_loc))
        Key = tuple([X_loc, Y_loc])
        if Key not in Loc_Map.keys():
            Loc_Map[Key] = 1
        else:
            Loc_Map[Key] = Loc_Map[Key] + 1
    
    data_list = np.zeros((Max_X_loc // 10 + 1, Max_Y_loc // 10 + 1))
    
    for k, v in Loc_Map.items():
        X_loc = int(k[0])
        Y_loc = int(k[1])
        data_list[X_loc // 10][Y_loc // 10] = data_list[X_loc // 10][Y_loc // 10] + int(v)
    
    plt.figure(figsize=(10, 10))
    ax = sns.heatmap(data_list, annot=False, cmap='YlGnBu')
    plt.xlabel('X location')
    plt.ylabel('Y location')
    plt.savefig(cloc_ + '/' + design + '_' + phase + '.png')
    plt.close()

    return

def bbox_overlap_coordinates(bbox_1, bbox_2):
    # bbox format
    # bbox = tuple([top_right, bottom_right, bottom_left, top_left])
    
    top_left_1 = bbox_1[3]
    top_left_2 = bbox_2[3]

    bottom_right_1 = bbox_1[1]
    bottom_right_2 = bbox_2[1]

    zero = tuple([0, 0])

    # determines the coordinates of the intersection rectangle
    x_left_intersection = max(top_left_1[0], top_left_2[0])
    y_top_intersection = min(top_left_1[1], top_left_2[1])
    x_right_intersection = min(bottom_right_1[0], bottom_right_2[0])
    y_bottom_intersection = max(bottom_right_1[1], bottom_right_2[1])

    # Check for validity of the bounding box
    if x_left_intersection > x_right_intersection or y_top_intersection < y_bottom_intersection:
        return tuple([tuple([zero, zero]), 0.0, False])
    
    # Calculate the top_left and bottom_right of the overlapping bounding box
    top_left = tuple([x_left_intersection, y_top_intersection])
    bottom_right = tuple([x_right_intersection, y_bottom_intersection])

    area = bbox_area(top_left, bottom_right)
    
    ## For cross verification of Bounding Box calculation
    '''
    try:
        area_verif = PolyVerif(bbox_1, bbox_2)
        assert(area == area_verif)
    except AssertionError:
        print('BUG in BBox calculation. Fix it')
        exit(0)
    '''
    is_overlap = True if area > 0.0 else False

    return tuple([tuple([top_left, bottom_right]), area, is_overlap])

'''
def PolyVerif(bbox_1, bbox_2):

    poly_1 = Poly(bbox_1)
    poly_2 = Poly(bbox_2)

    return poly_1.intersection(poly_2).area
'''

def bbox_area(top_left, bottom_right):

    return 1.0 * (bottom_right[0] - top_left[0]) * \
            (top_left[1] - bottom_right[1])

    return

def estimate_congestion_per_net(overlap_bbox_area_dict, overlap_bbox_area_true, bbox_area_dict, net_fo_map):
    # Estimated congestion per wire
    congestion_factor = {}

    nets = net_fo_map.keys()

    for net_1 in nets:
        sum_frac_overlapping_area = 0.0
        for net_2 in nets:
            if net_1 == net_2:
                continue
            else:
                net_pair = tuple([net_1, net_2])
                try:
                    overlap_bbox_area_dict_ = overlap_bbox_area_dict[net_pair]
                    #print(overlap_bbox_area_dict_)
                    o = True
                except KeyError:
                    #print('net_pair not found in overlap_bbox_area_dict: ' + str(net_pair))
                    o = False

                try:
                    net_fo_map_ = net_fo_map[net_2]
                    #print(net_fo_map_)
                    n = True
                except KeyError:
                    #print('net_fo_map error: ' + net_2)
                    n = False

                try:
                    bbox_area_dict_ = bbox_area_dict[net_2]
                    #print(bbox_area_dict_)
                    b = True
                except KeyError:
                    #print('bbox_area_dict error: ' + net_2)
                    b = False
    
                #if o and n and b and bbox_area_dict_ > 0.0:
                #    print(1.0 * overlap_bbox_area_dict_ * net_fo_map_ / bbox_area_dict_)

                #if not overlap_bbox_area_true[net_pair]:
                #    continue
                #else:
                if o and n and b and bbox_area_dict_ > 0.0:
                    sum_frac_overlapping_area = sum_frac_overlapping_area + \
                            1.0 * overlap_bbox_area_dict_ * net_fo_map_ / bbox_area_dict_
                            #1.0 * overlap_bbox_area_dict[net_pair] * net_fo_map[net_2] / bbox_area_dict[net_2]
        if bbox_area_dict[net_1] > 0.0 and net_fo_map[net_1] > 0:
            congestion_factor[net_1] = 1.0 * sum_frac_overlapping_area / \
                    ( 1.0 * bbox_area_dict[net_1] / net_fo_map[net_1])
        else:
            congestion_factor[net_1] = 0.0

    return congestion_factor

def construct_bboxes(net_points):
    # net_points is a dictionary where the key: the name of the net and the value is the list of coordinates
    # of the source node and destination nodes in the format [(38, 40), (60, 28), (47, 27), (28, 10)]
    
    bbox_dict = {}
    overlap_bbox_area_dict = {}
    overlap_bbox_area_true = {}
    bbox_area_dict = {}
    
    nets = net_points.keys()
    net_pairs = combinations(nets, 2)

    for net in nets:
        points = net_points[net]
        bbox = BBox(points)
        bbox_area_dict[net] = bbox_area(bbox[3], bbox[1])
        bbox_dict[net] = bbox


    for net_pair in net_pairs:
        result = bbox_overlap_coordinates(bbox_dict[net_pair[0]], bbox_dict[net_pair[1]])
        overlap_bbox_area_true[net_pair] = result[2]
        overlap_bbox_area_dict[net_pair] = result[1]
        del result

    #print('The size of bbox_area_dict: ' + str(len(bbox_area_dict)))
    #print('The size of overlap_bbox_area_dict: ' + str(len(overlap_bbox_area_dict)))
    
    return overlap_bbox_area_dict, overlap_bbox_area_true, bbox_area_dict


def BBox(points):

    try:
        assert(type(points) == list)
    except AssertionError:
        print('List of points not received. Bounding Box calculation not possible')
        return tuple([])

    bbox, dimension= bbox_(points)
    #p_h = 1.0 / dimension[1]
    #p_v = 1.0 / dimension[0]

    return bbox

def bbox_(Points):

    x_max = max([point[0] for point in Points])
    x_min = min([point[0] for point in Points])
    y_max = max([point[1] for point in Points])
    y_min = min([point[1] for point in Points])

    top_right = tuple([x_max, y_max])
    bottom_right = tuple([x_max, y_min])
    bottom_left = tuple([x_min, y_min])
    top_left = tuple([x_min, y_max]) 

    height = y_max - y_min
    width = x_max - x_min

    dimension = tuple([width, height])
    bbox = tuple([top_right, bottom_right, bottom_left, top_left])

    return bbox, dimension


def cross_verify_bbox():
    # This routine is to verify the bounding box routines
    # No use in main programming
    pic_dir = './box_graph'

    for i in range(100):
        bbox_1 = BBox([(np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10))
                       ])

        bbox_2 = BBox([(np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10)), \
                       (np.random.randint(1, 10), np.random.randint(1, 10))
                       ])

        print('Checking for iteration: ' + str(i))
        
        results = bbox_overlap_coordinates(bbox_1, bbox_2)

        coords = results[0]
        area = results[1]
        
        if area == 0.0:
            continue

        top_left_intersection = coords[0]
        bottom_right_intersection = coords[1]
        bbox = BBox([top_left_intersection, bottom_right_intersection, \
                    (top_left_intersection[0], bottom_right_intersection[1]), \
                    (bottom_right_intersection[0], top_left_intersection[1])
                    ])

        x1 = [ele[0] for ele in bbox_1]
        y1 = [ele[1] for ele in bbox_1]
        x2 = [ele[0] for ele in bbox_2]
        y2 = [ele[1] for ele in bbox_2]
        
        x = [ele[0] for ele in bbox]
        y = [ele[1] for ele in bbox]

        print(tuple([x1, y1]))
        print(tuple([x2, y2]))
        print(tuple([x, y]))

        plt.plot(x1, y1, 'b-')
        plt.plot(x2, y2, 'r--')
        plt.plot(x, y, 'm:')

        plt.fill_between(x1, min(y1), max(y1), color='red', alpha=0.5)
        plt.fill_between(x2, min(y2), max(y2), color='blue', alpha=0.6)
        plt.fill_between(x, min(y), max(y), color='green')

        plt.savefig(pic_dir + '/' + str(i) + '.png')
        plt.close()

    return

#cross_verify_bbox()

'''
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-c', '--coord', help="Cell Coordinate File location with absolute path", \
            dest='cloc', required=True)
    
    options = parser.parse_args()

    phases = ['place', 'route']
    designs = ['diffeq2', 'harris_1', 'harris_5', 'harris_6', 'sobel_1', 'gaussblur_1', 'paintmask_1']

    for design in designs:
        cloc_ = options.cloc + '/' + design
        for phase in phases:
            visualize(cloc_, \
                      design, \
                      phase)
'''
