#===============================================================================
#
#         FILE: extract_subgraph.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 27-01-2020
#     REVISION: 
#    LMODIFIED: Thu 20 Feb 2020 11:25:08 PM EST
#===============================================================================

import os, sys
import networkx as nx
from logic import logics_xcvu440_flga2892_2_e_group as lxcvu440_group
from plot_graph import fill_color
from plot_graph import plot_graph_ as pg
from plot_graph import make_directory as mkdir
from plot_graph import printTable2
from plot_graph import printTable3
from visualize import construct_bboxes, estimate_congestion_per_net 
import pprint as pp

from collections import OrderedDict as ODict 
#from PIL import Image, ImageDraw

def extract_subgraph(ngraph, MODINSTS, design, phase, wire_coord_map, gloc_, lloc_):
    
    # Extract the name of the multi-bit register groups from the MODINSTS module type and instance name
    icontent, gr_insts_by_type = group_registers(MODINSTS)
    
    reg_file_name = open(lloc_ + '/' + design + '_' + phase + '.reglist', 'w')
    reg_file_name.write('\n'.join(icontent))
    reg_file_name.close()

    reg_gr_file_name = open(lloc_ + '/' + design + '_' + phase + '.regrplist', 'w')
    for instance_ in gr_insts_by_type.keys():
        reg_gr_file_name.write('>' * 3 + ' ' * 3 + 'Reg: ' + instance_ + ' ' * 3 + '<' * 3 + '\n')
        reg_gr_file_name.write('\n'.join(gr_insts_by_type[instance_]))
        reg_gr_file_name.write('\n' * 2 )
    reg_gr_file_name.close()

    # Graph traversal
    subgraphs = traverse_graph(ngraph, gr_insts_by_type, design, phase, wire_coord_map, gloc_, lloc_)
    subgraph_fused, delay_dict = fuse_subgraphs(subgraphs, design, phase, lloc_, gloc_)

    return subgraphs, subgraph_fused, delay_dict

def fuse_subgraphs(subgraphs, design, phase, lloc_, gloc_):
    
    gloc__ = gloc_ + '/subgraph'
    mkdir(gloc__)

    delay_dict = {}
    node_dict = {}
    edge_dict = {}
    edge_num = 0

    subgraph_fused = nx.DiGraph()
    for subgraph in subgraphs:
        subgraph_fused = nx.compose(subgraph_fused, subgraph)

    fused_subgraph_summ = open(lloc_ + '/' + design + '_' + phase + '.fusedsumm', 'w')
    fused_subgraph_summ.write('Total number of nodes in the fused sub-graph: ' \
                               + str(len(subgraph_fused.nodes())) \
                               + '\n' + \
                               'Total number of edges in the fused sub-graph: ' \
                               + str(len(subgraph_fused.edges()))
                               )
    fused_subgraph_summ.close()
    
    for node in subgraph_fused.nodes():
        node_dict[node] = [subgraph_fused.nodes[node]['cell_type'], \
                           subgraph_fused.nodes[node]['coordinates'], \
                           subgraph_fused.nodes[node]['fanin'], \
                           subgraph_fused.nodes[node]['fanout'], \
                           subgraph_fused.nodes[node]['pred'], \
                           subgraph_fused.nodes[node]['succ'], \
                           subgraph_fused.nodes[node]['delay'], \
                           subgraph_fused.nodes[node]['congestion'], \
                           subgraph_fused.nodes[node]['slack'], \
                           subgraph_fused.nodes[node]['grtruth'], \
                           ]
                        

    for edge in subgraph_fused.edges():
        src = edge[0]
        dest = edge[1]
        delay_dict[str(edge_num)] = [subgraph_fused.edges[src, dest]['wire'].replace(' ', '').rstrip(), \
                                     dest + '/' + subgraph_fused.edges[src, dest]['dest_pin']
                                    ]
        edge_dict[str(edge_num)] = [src, \
                                    dest, \
                                    subgraph_fused.edges[src, dest]['wire'], \
                                    subgraph_fused.edges[src, dest]['delayp'], \
                                    subgraph_fused.edges[src, dest]['delayr'], \
                                    subgraph_fused.edges[src, dest]['delays'], \
                                    subgraph_fused.edges[src, dest]['cfactor'], \
                                    subgraph_fused.edges[src, dest]['weight'], \
                                    subgraph_fused.edges[src, dest]['src_pin'], \
                                    subgraph_fused.edges[src, dest]['dest_pin'], \
                                    subgraph_fused.edges[src, dest]['grtruth']
                                    ]

        edge_num = edge_num + 1

    pg(subgraph_fused, subgraph_fused.nodes(), subgraph_fused.edges(), lloc_ \
            + '/subgraph', 'subgraph_fused')

    node_content = printTable3(node_dict, ['Instance Name', 'Primitive Name', 'Coordinates', \
                                           'Fan In', 'Fan Out', 'Predecessor Node', \
                                           'Successor Node', 'Cell Delay', 'Congestion', 'Slack', \
                                           'Label'])
    nodefile = open(gloc__ + '/subgraph_fused.nodelist', 'w')
    nodefile.write(node_content)
    nodefile.close()

    edge_content = printTable3(edge_dict, ['Edge Num', 'Source Primitive', 'Dest Primitive', \
                                           'Wire Name', 'Delay Place', 'Delay Route', \
                                           'Delay Stretch', 'Congestion Factor', \
                                           'Weight', 'Source Pin', 'Destination Pin', \
                                           'Labels'])
    edgefile = open(gloc__ + '/subgraph_fused.edgelist', 'w')
    edgefile.write(edge_content)
    edgefile.close()

    del node_dict, edge_dict

    return subgraph_fused, delay_dict

def traverse_graph(ngraph, gr_insts_by_type, design, phase, wire_coord_map, gloc_, lloc_):
    # A list of 
    lloc__ = lloc_ + '/subgraph'
    mkdir(lloc__)

    gloc__ = gloc_ + '/subgraph'
    mkdir(gloc__)

    subgraphs = []
    subgraph_summary = {}
    for reg in gr_insts_by_type.keys():
        print('>' * 3 + ' Working on source Register: ' + reg)
        subgraph = nx.DiGraph()
        src_nodes = gr_insts_by_type[reg]
        for src_node in src_nodes:
            subgraph.add_node(src_node, \
                              cell_type=ngraph.nodes[src_node]['cell_type'], \
                              coordinates=ngraph.nodes[src_node]['coordinates'], \
                              fanin=0, \
                              fanout=0, \
                              pred=[], \
                              succ=[], \
                              delay=0.0, \
                              congestion=[0] * 10, \
                              slack=[0] * 10, \
                              grtruth=ngraph.nodes[src_node]['grtruth'], \
                              shape='doublecircle', \
                              style='filled', \
                              fillcolor=fill_color(ngraph.nodes[src_node]['cell_type']), \
                              label=src_node.replace('/', '_') + '\n' + \
                                    ngraph.nodes[src_node]['cell_type'], \
                              )
        node_dict, edge_dict = traverse_graph_(ngraph, src_nodes, subgraph)
        pg(subgraph, subgraph.nodes(), subgraph.edges(), lloc__, reg.replace('/', '_'))
        subgraphs.append(subgraph)

        node_content = printTable3(node_dict, ['Instance Name', 'Primitive Name', 'Coordinates', \
                                               'Fan In', 'Fan Out', 'Predecessor Node', \
                                               'Successor Node', 'Cell Delay', 'Congestion', 'Slack', \
                                               'Label'])
        nodefile = open(gloc__ + '/' + reg.replace('/', '_') + '.nodelist', 'w')
        nodefile.write(node_content)
        nodefile.close()

        edge_content = printTable3(edge_dict, ['Edge Num', 'Source Primitive', 'Dest Primitive', \
                                               'Wire Name', 'Delay Place', 'Delay Route', \
                                               'Delay Stretch', 'Congestion Factor', \
                                               'Weight', 'Source Pin', 'Destination Pin', \
                                               'Labels'])
        edgefile = open(gloc__ + '/' + reg.replace('/', '_') + '.edgelist', 'w')
        edgefile.write(edge_content)
        edgefile.close()

        subgraph_summary[reg] = [len(subgraph.nodes()), len(subgraph.edges())]

        del subgraph
    
    subgraph_summary = ODict(sorted(subgraph_summary.items(), key=lambda t: t[1], reverse=True))
    scontent = printTable2(subgraph_summary, ['Reg Name', 'Number of Nodes', 'Number of Edges'] ) 
    subgraph_summary_file = open(lloc_ + '/' + design + '_' + phase + '.subgraphsumm', 'w')
    subgraph_summary_file.write(scontent)
    subgraph_summary_file.close()

    return subgraphs

def traverse_graph_(ngraph, src_nodes, subgraph):
    # A helper routine for the netlist graph traversal between any pair of registers. 
    # We do it in a BFS manner. From each node we find its successor from ngraph, 
    # and append that node in the subgraph. If any path hits a register such as FDSE/FDRE
    # ( we check that via cell_type value of the node)
    # then we remove that bit from src_nodes. Tricky, so be careful
    
    node_dict = {}
    edge_dict = {}
    wire_coord_map = {}
    net_fo_map = {}
    edge_num = 0
    CONST_NEG_1 = '-1'

    NodeQueue = src_nodes
    while NodeQueue:
        src_node = NodeQueue.pop()
        succ_nodes = ngraph.successors(src_node)
        for succ_node in succ_nodes:
            is_terminal = False
            if ngraph.nodes[succ_node]['cell_type'] not in lxcvu440_group['REGISTER']:
                NodeQueue.append(succ_node)
            else:
                is_terminal = True
            
            if not subgraph.has_node(succ_node):
                subgraph.add_node(succ_node, \
                                  cell_type=ngraph.nodes[succ_node]['cell_type'], \
                                  coordinates=ngraph.nodes[succ_node]['coordinates'], \
                                  fanin=0, \
                                  fanout=0, \
                                  pred=[], \
                                  succ=[], \
                                  delay=0.0, \
                                  congestion=[0] * 10, \
                                  slack=[0] * 10, \
                                  grtruth=ngraph.nodes[succ_node]['grtruth'], \
                                  shape='box' if not is_terminal else 'doubleoctagon', \
                                  fillcolor=fill_color(ngraph.nodes[succ_node]['cell_type']), \
                                  style='filled', \
                                  label=succ_node.replace('/', '_') + '\n' + \
                                        ngraph.nodes[succ_node]['cell_type']
                                    )
            # NOTE: This is assert is to make sure that none of the labels
            #       have the place holder value of -1. They are either 0 or 1
            #       as Chenhui pointed put that he found many -1. Bug diagnosed and patched
            
            subgraph.add_edge(src_node, succ_node, \
                              wire=ngraph.edges[src_node, succ_node]['wire'], \
                              delayp=ngraph.edges[src_node, succ_node]['delayp'], \
                              delayr=ngraph.edges[src_node, succ_node]['delayr'], \
                              delays=ngraph.edges[src_node, succ_node]['delays'], \
                              cfactor=ngraph.edges[src_node, succ_node]['cfactor'], \
                              sfactor=ngraph.edges[src_node, succ_node]['sfactor'], \
                              weight=ngraph.edges[src_node, succ_node]['weight'], \
                              src_pin=ngraph.edges[src_node, succ_node]['src_pin'], \
                              dest_pin=ngraph.edges[src_node, succ_node]['dest_pin'], \
                              grtruth=ngraph.edges[src_node, succ_node]['grtruth']
                              )

    for edge in subgraph.edges():
        src_node = edge[0] 
        dest_node = edge[1] 
        var = subgraph.edges[src_node, dest_node]['wire']
        if var not in wire_coord_map.keys():
            wire_coord_map[var] = [subgraph.nodes[src_node]['coordinates'], \
                                    subgraph.nodes[dest_node]['coordinates']
                                    ]
        else:
            c_to_push = subgraph.nodes[dest_node]['coordinates']
            wire_coord_map[var].append(c_to_push)
    
    for var in wire_coord_map.keys():
        net_fo_map[var] = len(wire_coord_map[var])
    
    overlap_bbox_area_dict, overlap_bbox_area_true, bbox_area_dict = construct_bboxes(wire_coord_map)
    congestion_factor = estimate_congestion_per_net(overlap_bbox_area_dict, \
            overlap_bbox_area_true, bbox_area_dict, net_fo_map)


    #print('The size of net_fo_map: ' + str(len(net_fo_map.keys())))
    #print('The size of congestion factor: ' + str(len(congestion_factor.keys())))

    for edge in subgraph.edges():
        src_node = edge[0]
        succ_node = edge[1]
        var = subgraph.edges[src_node, succ_node]['wire']
        cfactor = congestion_factor[var]
        subgraph.edges[src_node, succ_node]['cfactor'] = cfactor
        # NOTE: For edgelist in Chenhui format
        edge_dict[str(edge_num)] = [src_node, \
                                    succ_node, \
                                    subgraph.edges[src_node, succ_node]['wire'], \
                                    subgraph.edges[src_node, succ_node]['delayp'], \
                                    subgraph.edges[src_node, succ_node]['delayr'], \
                                    subgraph.edges[src_node, succ_node]['delays'], \
                                    subgraph.edges[src_node, succ_node]['cfactor'], \
                                    subgraph.edges[src_node, succ_node]['sfactor'], \
                                    subgraph.edges[src_node, succ_node]['weight'], \
                                    subgraph.edges[src_node, succ_node]['src_pin'], \
                                    subgraph.edges[src_node, succ_node]['dest_pin'], \
                                    subgraph.edges[src_node, succ_node]['grtruth']
                                    ]

        edge_num = edge_num + 1           

    # NOTE: Once a sub-graph construction is complete, getting its successors and predecessors. 
    # NOTE: The successors and predecessors cannot be calculated from the parent graph because 
    # NOTE: for some nodes (like source nodes) the predecessors will get vanished and for some nodes
    # NOTE: (like sink nodes) the successors will get vanished. Safe to do after subgraph completion

    for node in subgraph.nodes():
        successors = [succ for succ in subgraph.successors(node)]
        predecessors = [pred for pred in subgraph.predecessors(node)]
        subgraph.nodes[node]['succ'] = 'EMPTY_FIELD' if not successors else ','.join(successors)
        subgraph.nodes[node]['pred'] = 'EMPTY_FIELD' if not predecessors else ','.join(predecessors)
        subgraph.nodes[node]['fanin'] = len(predecessors)
        subgraph.nodes[node]['fanout'] = len(successors)

        # NOTE: Finding the Congestion vector for a node based on its fan-in and fan-out edges
        cvector, svector = embed_congestion_slack(node, successors, predecessors, subgraph)
        subgraph.nodes[node]['congestion'] = ','.join(str(cvector_) for cvector_ in cvector)
        subgraph.nodes[node]['slack'] = ','.join(str(svector_) for svector_ in svector)
        subgraph.nodes[node]['coordinates'] = str(subgraph.nodes[node]['coordinates']).replace(' ', '')[1 : -1]

        # NOTE: For nodelist in Chenhui format
        node_dict[node] = [subgraph.nodes[node]['cell_type'], \
                           subgraph.nodes[node]['coordinates'], \
                           subgraph.nodes[node]['fanin'], \
                           subgraph.nodes[node]['fanout'], \
                           subgraph.nodes[node]['pred'], \
                           subgraph.nodes[node]['succ'], \
                           subgraph.nodes[node]['delay'], \
                           subgraph.nodes[node]['congestion'], \
                           subgraph.nodes[node]['slack'], \
                           subgraph.nodes[node]['grtruth']
                           ]

        del successors
        del predecessors

    return node_dict, edge_dict

def embed_congestion_slack(node, successors, predecessors, graph):
    cvector_pred = []
    cvector_succ = []

    svector_pred = []
    svector_succ = []

    for succ in successors:
        cvector_succ.append(graph.edges[node, succ]['cfactor'])

    for pred in predecessors:
        cvector_pred.append(graph.edges[pred, node]['cfactor'])
    
    for succ in successors:
        svector_succ.append(graph.edges[node, succ]['sfactor'])

    for pred in predecessors:
        svector_pred.append(graph.edges[pred, node]['sfactor'])

    cvector_pred = sorted(cvector_pred, reverse=True)
    cvector_succ = sorted(cvector_succ, reverse=True)

    svector_pred = sorted(svector_pred)
    svector_succ = sorted(svector_succ)

    cvector_pred = cvector_pred[0:5] if len(cvector_pred) >= 5 else cvector_pred + [0] * (5 - len(cvector_pred))
    cvector_succ = cvector_succ[0:5] if len(cvector_succ) >= 5 else cvector_succ + [0] * (5 - len(cvector_succ))

    svector_pred = svector_pred[0:5] if len(svector_pred) >= 5 else svector_pred + [0] * (5 - len(svector_pred))
    svector_succ = svector_succ[0:5] if len(svector_succ) >= 5 else svector_succ + [0] * (5 - len(svector_succ))


    cvector = cvector_pred + cvector_succ

    svector = svector_pred + svector_succ

    del cvector_pred
    del cvector_succ

    del svector_pred
    del svector_succ

    return cvector, svector

def group_registers(MODINSTS):

    reg_modules = lxcvu440_group['REGISTER']
    gr_insts_by_type = {}
    icontent = []

    for instance in MODINSTS.keys():
        module = MODINSTS[instance][0]
        icontent.append(instance)

        if module in reg_modules:
            is_part_of_bus = instance.find('[') != -1 
            if is_part_of_bus:
                instance_ = instance[:instance.find('[')]
                if instance_ not in gr_insts_by_type.keys():
                    gr_insts_by_type[instance_] = [instance]
                else:
                    gr_insts_by_type[instance_].append(instance)

    if gr_insts_by_type:
        for instance_ in gr_insts_by_type.keys():
            gr_insts_by_type[instance_] = sorted(gr_insts_by_type[instance_])
    
    return icontent, gr_insts_by_type

# Code copied from https://note.nkmk.me/en/python-pillow-gif/
def convert_to_gif():
    # TODO: The code will be here soon
    return
