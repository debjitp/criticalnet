#===============================================================================
#
#         FILE: plot_graph.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 15-01-2020
#     REVISION: 
#    LMODIFIED: Thu 30 Jan 2020 05:04:33 PM EST
#===============================================================================

import os, errno
from networkx.drawing.nx_agraph import *
from logic import Logic_FillColor as FillColor
import subprocess as sbp
from collections import OrderedDict as ODict

def plot_graph(ngraph, gloc, design, phase, Primitives):

    gloc_ = gloc + '/' + design
    make_directory(gloc_)
    
    dep_file = gloc_ + '/' + design + '_' + phase + '.dep'
    node_info = ngraph.nodes()
    edge_info = ngraph.edges()

    econtent = [i[0] + '\t' + i[1] for i in edge_info]
    dep_handle = open(dep_file, 'w')
    dep_handle.write('\n'.join(econtent))
    dep_handle.close()

    dep_summary = gloc_ + '/' + design + '_' + phase + '.summary'
    dep_summ_handle = open(dep_summary, 'w')
    dep_summ_handle.write(
            'Total number of nodes in the netlist graph: ' + \
                    str(len(node_info)) + '\n' + \
                    'Total number of edges in the netlist graph: ' + \
                    str(len(edge_info))
                    )
    dep_summ_handle.close()

    primitives = {}
    for key in Primitives.keys():
        primitives[key] = len(Primitives[key])


    primitives['TOTAL'] = sum([primitives[key] for key in primitives.keys()])
    
    primitives = ODict(sorted(primitives.items(), key=lambda t: t[1]))
    pcontent = printTable1(primitives, ['Primitive name', 'Primitive use count'])
    prim_file = gloc_ + '/' + design + '_' + phase + '.prim'
    prim_handle = open(prim_file, 'w')
    prim_handle.write(pcontent)
    prim_handle.close()

    plot_graph_(ngraph, node_info, edge_info, gloc_, design + '_' + phase)

    return

def plot_graph_(ngraph, node_info, edge_info, loc, name):
    
    dot_file_name = loc + '/' + name + '.dot' 
    A = to_agraph(ngraph)
    A.layout()
    A.draw(dot_file_name)

    if len(node_info) < 600 and len(edge_info) < 2000:
        #write_dot(ngraph, dot_file_name)
        file_type = ['pdf']
        for typ in file_type:
            typ_file_name = dot_file_name[:dot_file_name.rfind('.')] + '.' + typ
            dot_cmd = 'dot -T' + typ + ' ' + dot_file_name + ' -o ' + typ_file_name
            sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)
    return

def printTable1(myDict, colList=None):
    if not colList:
        colList = list(myDict[0].keys() if myDict else []) 
    myList = [colList]
    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])
    colSize = [max(map(len, col)) for col in zip(*myList)]
    formatStr = '| ' + ' | '.join(["{{:^{}}}".format(i) for i in colSize]) + ' |'
    myList.insert(0, ['-' * i for i in colSize])
    myList.insert(2, ['-' * i for i in colSize])
    myList.insert(len(myDict) + 2, ['-' * i for i in colSize])
    myList.insert(len(myDict) + 4, ['-' * i for i in colSize])
    
    content = ''
    for item in myList:
        content = content + formatStr.format(*item) + '\n'
    return content

def printTable2(myDict, colList=None):
    if not colList:
        colList = list(myDict[0].keys() if myDict else [])
    myList = [colList]

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    formatStr = ' | '.join(["{{:^{}}}".format(i) for i in colSize])
    myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content

def printTable3(myDict, colList=None):
    #if not colList:
    #    colList = list(myDict[0].keys() if myDict else [])
    #myList = [colList]
    myList = []

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    #formatStr = '\t '.join(["{{:^{}}}".format(i) for i in colSize])
    formatStr = ' '.join(["{{:^{}}}".format(i) for i in range(len(colSize))])
    #myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content

def make_directory(dir_path):

    if not os.path.exists(dir_path):
        try:
            os.makedirs(dir_path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    return

def fill_color(typ):
    try:
        return FillColor[typ]
    except KeyError:
        return FillColor['Default']

