#===============================================================================
#
#         FILE: logic.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 14-01-2020
#     REVISION: 
#    LMODIFIED: Wed 12 Feb 2020 01:18:18 PM EST
#===============================================================================

import os, sys

logics_xcvu440_flga2892_2_e = {
        # Cross checked with EDIF
        # Not a functional unit
        'BUFGCE': {'IN': ['CE', 'I'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'CARRY8': {'IN': ['CI', 'CI_TOP', 'DI', 'S'], 'OUT': ['CO', 'O']},
        # Cross checked with EDIF
        # A functional unit
        'DSP48E2': {'IN': ['CARRYCASCIN', 'CARRYIN', 'CEA1', 'CEA2', 'CEAD', 'CEALUMODE', \
                           'CEB1', 'CEB2', 'CEC', 'CECARRYIN', 'CECTRL', 'CED', 'CEINMODE', \
                           'CEM', 'CEP', 'CLK', 'MULTSIGNIN', 'RSTA', 'RSTALLCARRYIN', \
                           'RSTALUMODE', 'RSTB', 'RSTC', 'RSTCTRL', 'RSTD', 'RSTINMODE', \
                           'RSTM', 'RSTP', 'ACIN', 'ALUMODE', 'A', 'BCIN', 'B', \
                           'CARRYINSEL', 'C', 'D', 'INMODE', 'OPMODE', 'PCIN'], \
                    'OUT': ['CARRYCASCOUT', 'MULTSIGNOUT', 'OVERFLOW', 'PATTERNBDETECT', \
                            'PATTERNDETECT', 'UNDERFLOW', 'ACOUT', 'BCOUT', 'CARRYOUT', \
                            'PCOUT', 'P', 'XOROUT']},
        # Cross checked with EDIF
        # Not a functional unit
        'FDRE': {'IN': ['C', 'CE', 'D', 'R'], 'OUT': ['Q']},
        # Cross checked with EDIF
        # Not a functional unit
        'FDSE': {'IN': ['S', 'D', 'CE', 'C'], 'OUT': ['Q']},
        # Cross checked with EDIF
        # Not a functional unit
        'GND': {'IN': [''], 'OUT': ['G']},
        # Cross checked with EDIF
        # Not a functional unit
        'IBUF': {'IN': ['I'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'INV': {'IN': ['I'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT1': {'IN': ['I0'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT2': {'IN': ['I0', 'I1'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT3': {'IN': ['I0', 'I1', 'I2'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT4': {'IN': ['I0', 'I1', 'I2', 'I3'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT5': {'IN': ['I0', 'I1', 'I2', 'I3', 'I4'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'LUT6': {'IN': ['I0', 'I1', 'I2', 'I3', 'I4', 'I5'], 'OUT': ['O']},
        # Cross checked with EDIF
        # Not a functional unit
        'OBUF': {'IN': ['I'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'RAMB18E2': {'IN': ['ADDRENA', 'ADDRENB', 'CASDIMUXA', 'CASDIMUXB', \
                            'CASDOMUXA', 'CASDOMUXB', \
                            'CASDOMUXEN_A', 'CASDOMUXEN_B', 'CASOREGIMUXA', 'CASOREGIMUXB', \
                            'CASOREGIMUXEN_A', 'CASOREGIMUXEN_B', 'CLKARDCLK', 'CLKBWRCLK', \
                            'ENARDEN', 'ENBWREN', 'REGCEAREGCE', 'REGCEB', 'RSTRAMARSTRAM', \
                            'RSTRAMB', 'RSTREGARSTREG', 'RSTREGB', 'SLEEP', 'ADDRARDADDR', \
                            'ADDRBWRADDR', 'CASDINA', 'CASDINB', 'CASDINPA', \
                            'CASDINPB', 'DINADIN', \
                            'DINBDIN', 'DINPADINP', 'DINPBDINP', 'WEA', 'WEBWE'], \
                    'OUT': ['CASDOUTA', 'CASDOUTB', 'CASDOUTPA', 'CASDOUTPB', \
                            'DOUTADOUT', 'DOUTBDOUT', \
                            'DOUTPADOUTP', 'DOUTPBDOUTP']},
        # Cross checked with EDIF
        # A functional unit
        'SRL16E': {'IN': ['D', 'CLK', 'CE', 'A3', 'A2', 'A1', 'A0'], 'OUT': ['Q']},
        # Cross checked with EDIF
        # A functional unit
        'SRLC32E': {'IN': ['A', 'D', 'CLK', 'CE'], 'OUT': ['Q31', 'Q']},
        # Cross checked with EDIF
        'VCC': {'IN': [''], 'OUT': ['P']},
        # Cross checked with EDIF
        # A functional unit
        'MUXF7': {'IN': ['I0', 'I1', 'S'], 'OUT': ['O']},
        # Cross checked with EDIF
        # A functional unit
        'MUXF8': {'IN': ['I0', 'I1', 'S'], 'OUT': ['O']}
        }


# Group of the different primitives
logics_xcvu440_flga2892_2_e_group = {
        'CLOCK': ['BUFGCE'], 
        'CLB': ['CARRY8', 'LUT1', 'LUT2', 'LUT3', 'LUT4', 'LUT5', 'LUT6', 'SRL16E'], 
        'ARITHMETIC': ['DSP48E2'],
        'REGISTER': ['FDRE', 'FDSE'],
        'IO': ['IBUF', 'OBUF'],
        'BLOCKRAM': ['RAMB18E2']
        }

Logic_FillColor = {
        'BUFGCE': 'springgreen', 
        'CARRY8': 'linen',
        'DSP48E2': 'lightgray',
        'FDRE': 'lightcyan',
        'FDSE': 'cadetblue1',
        'GND': 'turquoise',
        'IBUF': 'firebrick',
        'LUT1': 'cadetblue',
        'LUT2': 'deepskyblue',
        'LUT3': 'gold',
        'LUT4': 'aquamarine',
        'LUT5': 'darkseagreen1',
        'LUT6': 'azure',
        'OBUF': 'crimson',
        'RAMB18E2': 'forestgreen',
        'SRL16E': 'darkgreen', 
        'SRLC32E': 'greenyellow',
        'VCC': 'cornflowerblue',
        'MUXF7': 'yellow',
        'MUXF8': 'yellow'
        }

