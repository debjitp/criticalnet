import os, sys, fnmatch
import pprint as pp
import regex as re
import networkx as nx
from networkx.drawing.nx_agraph import *
import subprocess as sbp
from collections import OrderedDict as ODict
from joblib import Parallel, delayed
import multiprocessing as mps
import numpy as np
from plot_graph import make_directory as mkdir
from plot_graph import printTable2, printTable3


NUMBER_OF_PROCESSES =  1 #mps.cpu_count()

unary_op = ['trunc', 'sext', 'zext', 'fptrunc', 'fpext', 'fptoui', 'fptosi']
nary_op = ['mul', 'add', 'sub']

FillColor = {
        'add': 'springgreen',
        'sub': 'cadetblue',
        'mul': 'red',
        'Default': 'white'
        }

def fill_color(typ):
    if 'mul' in typ:
        return FillColor['mul']
    try:
        return FillColor[typ]
    except KeyError:
        return FillColor['Default']

from parse_schedule_report import ScheduleReportParser

def analyze(report):
    sched_parser = ScheduleReportParser(report)
    sched_parser.parse()
    comp_info = sched_parser.comp_info
    net_info = sched_parser.net_info
    
    return comp_info, net_info

def build_op_graph(comp_info, dest_src):

    #pp.pprint(comp_info)
    #pp.pprint(dest_src)

    ograph = nx.DiGraph()
    ograph_add = nx.DiGraph()

    op_pattern = re.compile(r'([0-9]+)([a-zA-Z]+)')

    var_def = {}
    var_use = {}

    var_def_add = {}
    var_use_add = {}
    add_op_dict = ODict()
    op_dict = ODict()

    output = []
    output_add = []
    '''
    796: {'func': 'or_ln852_3_fu_796',
       'id': 796,
       'ops': ['or_ln852_3'],
       'pins': [{'bw': 1, 'dir': 0, 'index': 0, 'nets': [194]},
                {'bw': 1, 'dir': 0, 'index': 1, 'nets': [195]},
                {'bw': 1, 'dir': 1, 'index': 2, 'nets': [200]}],
       'type': '1004or'},
    '''
    for key in comp_info.keys():
        op_opcode = comp_info[key]['type']
        match = re.search(op_pattern, op_opcode)
        if match:
            op_name = match.group(2)
            pins = comp_info[key]['pins']
            for pin in pins:
                if pin['dir'] == 1:
                    try:
                        obw = pin['bw']
                    except KeyError:
                        obw = 0
                    break
            ograph.add_node(op_name + str(key),\
                       obw=obw, \
                       remove=True if 'mul' in op_name else False, \
                       fillcolor=fill_color(op_name), \
                       style='filled', \
                       label=op_name + str(key), \
                       )

            op_dict[op_name + ':' + str(key)] = []
            if op_name == 'add' or op_name == 'sub':
                ograph_add.add_node(op_name + str(key), \
                        label=op_name + str(key)
                        )
                add_op_dict[op_name + str(key)] = []

            ops = comp_info[key]['ops']
            pins = comp_info[key]['pins']
            #for op in ops:
            if ops:
                # NOTE: Hard assumtimption that last element of the opset is the final output
                #       of the operator
                op = ops[-1]
                var_def[op] = [op_name + str(key)]
                op_dict[op_name + ':' + str(key)].append(op)
                op_dict[op_name + ':' + str(key)].append(pins[-1]['bw'])
                if op_name == 'add' or op_name == 'sub':
                    var_def_add[op] = [op_name + str(key)]
                    add_op_dict[op_name + str(key)].append(op)
                    output_add.append(op)

            src_comps = dest_src[key]
            use_var_add = []
            use_var_op = []
            for src_comp in src_comps:
                # NOTE: Hard assumtimption that last element of the opset is the one that is used
                #       in the following operator
                try:
                    use_var = comp_info[src_comp]['ops'][-1] 
                except IndexError:
                    continue

                if use_var not in var_use.keys():
                    var_use[use_var] = [op_name + str(key)]
                else:
                    var_use[use_var].append(op_name + str(key))

                if use_var not in var_use_add.keys():
                    if op_name == 'add' or op_name == 'sub':
                        var_use_add[use_var] = [op_name + str(key)]
                else:
                    if op_name == 'add' or op_name == 'sub':
                        var_use_add[use_var].append(op_name + str(key))
                
                use_var_op.append(use_var)

                if op_name == 'add' or op_name == 'sub':
                    use_var_add.append(use_var)
            op_dict[op_name + ':' + str(key)].append(use_var_op)
            
            if op_name == 'add' or op_name == 'sub':
                add_op_dict[op_name + str(key)].append(use_var_add)
            del use_var_add
            del use_var_op

    ograph.add_node('sink', remove=False, style='invis', obw=0)
    
    EdgeLabelDict = {}

    for def_var in var_def.keys():
        dins = var_def[def_var]
        for din in dins:
            try:
                uins = var_use[def_var]
            except KeyError:
                output.append(def_var)
                uins = ['sink']
            for uin in uins:
                if not ograph.has_edge(din, uin):
                    ograph.add_edge(din, uin, \
                            label=def_var)
                    EdgeLabelDict[def_var] = [din, uin]

    ograph_add.add_node('source', style='invis')
    ograph_add.add_node('sink', style='invis')

    for def_var_add in var_def_add.keys():
        dins = var_def_add[def_var_add]
        for din in dins:
            try:
                uins = var_use_add[def_var_add]
            except KeyError:
                uins = ['sink']
            for uin in uins:
                if not ograph_add.has_edge(din, uin):
                    ograph_add.add_edge(din, uin, \
                            label=def_var_add)
    
    #pp.pprint(var_def)
    #pp.pprint(var_use)
    return ograph, add_op_dict, op_dict, var_def, var_use, output_add, EdgeLabelDict

def get_file_names_from_loc(root_dir, extension):
    name_of_files = []
    for root, dirNames, fileNames in os.walk(root_dir):
        for fileName in fnmatch.filter(fileNames, extension):
            name_of_files.append(os.path.abspath(os.path.join(root, fileName)))

    return name_of_files

def build_dep_grpah(dest_src, comp_info):

    op_pattern = re.compile(r'([0-9]+)([a-zA-Z]+)')
    
    dep_g = nx.DiGraph()
    # NOTE: The rationale behind this dependency graph follows the same idea as that of the
    #       add_op graph. That the last element in the opset of the comp is the one that is finally
    #       defined and hence as well used. And the input to that comp are the use variables
    #       that defines the output.
    for dkey in dest_src.keys():
        #dvar = comp_info[dkey]['ops'][0]
        dvar = comp_info[dkey]['ops'][-1]
        op_opcode = comp_info[dkey]['type']
        match = re.search(op_pattern, op_opcode)
        if match:
            op_name = match.group(2)
        #for dvar in dvars:
        if not dep_g.has_node(dvar):
            dep_g.add_node(dvar, \
                           d_by=op_name + str(dkey), \
                           stop=True if 'mul' in op_name else False, \
                           fillcolor='red' if 'mul' in op_name else 'white', \
                           style='filled', \
                           visited=0)
        ukeys = dest_src[dkey]
        for ukey in ukeys:
            #uvars = comp_info[ukey]['ops']
            try:
                uvars = [comp_info[ukey]['ops'][-1]]
            except IndexError:
                uvars = []
            for uvar in uvars:
                op_opcode = comp_info[ukey]['type']
                match = re.search(op_pattern, op_opcode)
                if match:
                    op_name = match.group(2)
                if not dep_g.has_node(uvar):
                    dep_g.add_node(uvar, \
                                   d_by=op_name + str(ukey), \
                                   stop=True if 'mul' in op_name else False, \
                                   fillcolor='red' if 'mul' in op_name else 'white', \
                                   style='filled', \
                                   visited=False)
                if not dep_g.has_edge(uvar, dvar):
                    dep_g.add_edge(uvar, dvar, \
                            # Used by which operator
                            u_by=dep_g.nodes[dvar]['d_by'], \
                            # Defined by which operator
                            d_by=dep_g.nodes[uvar]['d_by'], \
                            label=dep_g.nodes[dvar]['d_by'] + '/' + dep_g.nodes[uvar]['d_by'])

    return dep_g

def extract_coi(dep_g, output_add, def_vars):

    op_pattern = re.compile(r'([a-zA-Z]+)([0-9]+)')

    preds = {}

    for node in dep_g.nodes():
        preds[node] = []
        for i in dep_g.predecessors(node):
            d_by = dep_g.edges[i, node]['d_by']
            match = re.search(op_pattern, d_by)
            preds[node].append([i, match.group(1)])

    #pp.pprint(preds)
    coi_dict = parallel_extract_coi_nodes(output_add, preds, def_vars)

    #for node in COI_labels.keys():
    #    coi_dict[node] = [', '.join(COI_labels[node]) if COI_labels[node] else 'EMPTY']

    return coi_dict

def parallel_extract_coi_nodes(output_add, preds, def_vars):

    num_output_lists = np.array_split(range(len(output_add)), NUMBER_OF_PROCESSES)

    coi_results = Parallel(n_jobs=NUMBER_OF_PROCESSES, require='sharedmem')(
            delayed(extract_coi_nodes)([output_add[idx] for idx in j], \
                    output_add, \
                    preds, \
                    def_vars
                    ) for
            i, j
            in enumerate(num_output_lists, 1))
    COI_labels = {}
    for coi_result in coi_results:
        if coi_result:
            COI_labels.update(coi_result)
    
    #pp.pprint(COI_labels)
    return COI_labels

def extract_coi_nodes(outps, output_add, preds, def_vars):
    COI = {}
    for outp in outps:
        COI_ = []
        NodeQueue = [outp]

        while NodeQueue:
            curr_node = NodeQueue.pop()
            in_coi = [j for j in range(len(preds[curr_node]))]
            to_explore = [preds[curr_node][j][0] for j in in_coi \
                    if preds[curr_node][j][1] not in nary_op and preds[curr_node][j][0] not in COI_]
            COI_ = COI_ + [preds[curr_node][j][0] for j in in_coi if preds[curr_node][j][0] not in COI_]
            NodeQueue.extend(to_explore)
            del in_coi
            del to_explore
        '''
        COI[outp] = [list(set(COI_).intersection(set(def_vars))), \
                     list(set(COI_).intersection(set(output_add)))
                     ]
        '''

        COI[outp] = COI_

        del COI_

    return COI

def find_hls_cluster(report):
    freport = open(report, 'r')
    lines = freport.readlines()
    freport.close()

    GROUP = 'grouped into TernaryAdder'
    ROOT = 'root node of TernaryAdder'
    SEMICOLON = '"'

    index = 1 
    GROUPED = []
    ROOTED = {}
    CLUSTER = []

    for line in lines:
        if GROUP in line:
            START_COLON = line.index(SEMICOLON)
            END_COLON = line[START_COLON + 1:].index(SEMICOLON)
            ele = line[START_COLON + 1: START_COLON + 1 + END_COLON].split()
            GROUPED.append(ele[0].strip())
        elif ROOT in line:
            START_COLON = line.index(SEMICOLON)
            END_COLON = line[START_COLON + 1:].index(SEMICOLON)
            ele = line[START_COLON + 1: START_COLON + 1 + END_COLON].split()
            ROOTED[ele[4].strip(',')] = ele[0]
            ROOTED[ele[5].strip()] = ele[0]
    for ele in GROUPED:
        CLUSTER.append([ele.strip('%'), ROOTED[ele].strip('%')])

    return CLUSTER

def extract_hls_graph(root_path, design, gloc_):
    rpt_path = 'project/solution/.autopilot/db'
    reports = get_file_names_from_loc(os.path.join(root_path, design, rpt_path), \
            '*.verbose.bind.rpt')
    
    design_adder_chain = os.path.join(gloc_, 'hls_adder_chain')
    mkdir(design_adder_chain)

    ADD_OP_DICT = {}
    DEF_OP_USE = {}
    DEP_G = {}
    EdgeLabelDict = {}
    COI_DICT = {}
    OGRAPH = {}
    HLS_CLUSTER = {}

    for report in reports:
        # NOTE: Uncomment the following line later
        #print('Analyzing report: ', report)
        comp_info, net_info = analyze(report)
        #pp.pprint(comp_info)
        #pp.pprint(net_info)

        dest_src = {}
    
        for i in range(len(net_info)):
            dic = net_info[i]
            dest_comp = dic['dest_comp']
            src_comp = dic['src_comp']
            if dest_comp not in dest_src.keys():
                dest_src[dest_comp] = [src_comp]
            else:
                dest_src[dest_comp].append(src_comp)
        #pp.pprint(dest_src) 
        ograph, add_op_dict, op_dict, var_def, var_use, \
                output_add, EdgeLabelDict_ = build_op_graph(comp_info, dest_src)
        
        ## Making a dictionary with defined variable as the key
        def_op_use = ODict()
        for key in op_dict.keys():
            def_var = op_dict[key][0]
            use_vars = op_dict[key][2]
            bw = op_dict[key][1]
            operation = key.split(':')
            def_op_use[def_var] = [operation[0], use_vars, int(operation[1]), bw]

        start_idx = report.rfind('/')
        end_idx = report[start_idx + 1 :].find('.')
        module = report[start_idx + 1 : start_idx + 1 + end_idx] 

        efile = open(os.path.join(design_adder_chain, module + '.edgelist'), 'w')
        efile.write('\n'.join([edge[0] + '\t' + edge[1] for edge in ograph.edges()]))
        efile.close()

        nfile = open(os.path.join(design_adder_chain, module + '.nodelist'), 'w')
        nfile.write('\n'.join([node + '\t' + str(ograph.nodes[node]['obw']) for node in ograph.nodes()]))
        nfile.close()


        f = open(os.path.join(design_adder_chain, module + '.txt'), 'w')
        acontent = printTable2(def_op_use, ['LValue', 'Operation', 'RValue(s)', 'Comp ID', 'Bit Width'])
        f.write(acontent)
        f.write('\n' * 3)
        
        if output_add:
            f.write('HLS add operations\n\n')
            temp = {}
            for add in output_add:
                temp[add] = [def_op_use[add][0], ','.join(def_op_use[add][1]), def_op_use[add][2], \
                        def_op_use[add][3]]
            add_op_dict = ODict(sorted(temp.items(), \
                    key=lambda t: t[1][2]))
            acontent = printTable2(add_op_dict, ['LValue', 'Operation', 'Operands', 'Comp ID', 'Bit Width'])
            f.write(acontent)
            del temp
        f.close()
         
        dot_file_name = os.path.join(design_adder_chain, module + '.dot')
        out_file_name = os.path.join(design_adder_chain, module + '.pdf')

        A = to_agraph(ograph)
        A.layout()
        A.draw(dot_file_name)

        dot_cmd = 'dot -Tpdf ' + dot_file_name + ' -o ' + out_file_name
        sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

        if output_add:
            nodes = [node for node in ograph.nodes()][:]
            for node in nodes:
                if ograph.nodes[node]['remove']:
                    ograph.remove_node(node)
        
        dot_file_name = os.path.join(design_adder_chain, module + '_mul_removed.dot')
        out_file_name = os.path.join(design_adder_chain, module + '_mul_removed.pdf')

        A = to_agraph(ograph)
        A.layout()
        A.draw(dot_file_name)

        dot_cmd = 'dot -Tpdf ' + dot_file_name + ' -o ' + out_file_name
        sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

        # NOTE: Two most important data structures coming out of this analysis are
        #       i)  add_op_dict
        #       ii) def_op_use

        DEF_OP_USE[module] = def_op_use
        if output_add:
            ADD_OP_DICT[module] = add_op_dict
            dep_g = build_dep_grpah(dest_src, comp_info)
            DEP_G[module] = dep_g
            EdgeLabelDict[module] = EdgeLabelDict_
            OGRAPH[module] = ograph
            coi_dict_ = extract_coi(dep_g, output_add, var_def.keys())
            coi_dict = {}
            for add in output_add:
                coi_dict[add_op_dict[add][0] + str(add_op_dict[add][2])] = coi_dict_[add]
            #print(nx.has_path(dep_g, 'add_ln31', 'a18'))
            COI_DICT[module] = ODict(sorted(coi_dict.items(), key=lambda t: int(t[0][3:])))

            dot_dep_file_name = os.path.join(design_adder_chain, module + '_dep.dot')
            out_dep_file_name = os.path.join(design_adder_chain, module + '_dep.pdf')

            A = to_agraph(dep_g)
            A.layout()
            A.draw(dot_dep_file_name)
    
            dot_cmd = 'dot -Tpdf ' + dot_dep_file_name + ' -o ' + out_dep_file_name
            sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

            lfile = open(os.path.join(design_adder_chain, module + '.edgename'), 'w')
            lfile.write('\n'.join(list(set([dep_g.edges[edge[0], edge[1]]['d_by'] + ' ' + edge[0] \
                    for edge in dep_g.edges()]))))
            lfile.close()

            hls_add_outps = find_hls_cluster(report)
            HLS_CLUSTER_ = []
            for hac in hls_add_outps:
                HLS_CLUSTER_.append([dep_g.edges[hac[0], hac[1]]['d_by'], \
                                    dep_g.edges[hac[0], hac[1]]['u_by']
                                    ]
                                    )
            HLS_CLUSTER[module] = HLS_CLUSTER_

            del HLS_CLUSTER_
            del dep_g
            del coi_dict
            del coi_dict_


        #if output_add:
        #    del coi_dict
        del def_op_use
        del output_add
        del var_use
        del var_def
        del op_dict
        del add_op_dict
        del ograph
        del EdgeLabelDict_

    HLS_ADD_FEATURE_ESTIMATION = {}

    #rpt_path = 'proj/sol1/.autopilot/db'
    rpt_path = 'project/solution/.autopilot/db'
    reports = get_file_names_from_loc(os.path.join(root_path, design, rpt_path), \
            '*.verbose.rpt')

    for report in reports:
        
        start_idx = report.rfind('/')
        end_idx = report[start_idx + 1 :].find('.')
        module = report[start_idx + 1 : start_idx + 1 + end_idx] 

        try:
            add_op_dict = ADD_OP_DICT[module]
        except KeyError:
            continue

        HLS_ADD_FEATURES = {}
        freport = open(report, 'r')
        lines = freport.readlines()
        freport.close()

        EXPRESSION = '* Functional unit list:'
        DASH = '-' * 10

        EXPRESSION_idx = -1

        for idx in range(len(lines)):
            if EXPRESSION in lines[idx].strip():
                EXPRESSION_idx = idx
                break
    
        curr_idx = EXPRESSION_idx + 4
        add_found = False
        consumed = False
        
        HLS_ADD_FEATURES_ = {}
        while not add_found:
            lines_ = []
            start_idx = curr_idx
            for i in range(start_idx, len(lines)):
                curr_idx = i
                if DASH in lines[i]:
                    curr_idx = curr_idx + 1
                    break
                eles = lines[i].strip().split('|')
                lines_.append(eles)
                if eles[1].strip() == 'add' or eles[1].strip() == 'sub':
                    add_found = True
                if eles[1].strip() == 'Total':
                    consumed = True
            if consumed:
                break

            if not add_found:
                continue

            for i in lines_:
                HLS_ADD_FEATURES_[i[2].strip()] = {'LUT': int(i[-2].strip()), \
                                                  'bitP0': 0
                                                  }
            add_found = False

        hls_output_sorted = sorted(add_op_dict.keys(), \
                                   key=lambda t: len(t), \
                                   reverse=True
                                   )

        feature_outputs = HLS_ADD_FEATURES_.keys()
        for feature_output in feature_outputs:
            for hls_output in hls_output_sorted:
                try:
                    index = feature_output.index(hls_output)
                except ValueError:
                    continue
                if index == 0:
                    HLS_ADD_FEATURES[hls_output] = {'LUT': HLS_ADD_FEATURES_[feature_output]['LUT'], \
                                                    'bitP0': DEF_OP_USE[module][hls_output][3]
                                                    }
        '''
        if EXPRESSION_idx != -1 and TOTAL_idx != -1:
            for idx in range(EXPRESSION_idx + 3, TOTAL_idx):
                if PLUSDASH in lines[idx]:
                    continue
                else:
                    matches = re.findall(LINE_pattern, lines[idx])
                    if matches and len(matches) == 7:
                        for i in range(len(matches)):
                            output = matches[0]
                            operation = matches[1]
                            LUT = int(matches[4])
                            bitP0 = int(matches[5])
                            bitP1 = int(matches[6])
                        hls_output_sorted = sorted(add_op_dict.keys(), \
                                                   key=lambda t: len(t), \
                                                   reverse=True
                                                   )
                        for hls_output in hls_output_sorted:
                            try:
                                index = output.index(hls_output)
                            except ValueError:
                                continue

                            if index == 0:
                                output = hls_output
                                break

                        HLS_ADD_FEATURES[output] = {'op': operation, \
                                                    'LUT': LUT, \
                                                    'bitP0': bitP0, \
                                                    'bitP1': bitP1
                                                    }
        '''
        if HLS_ADD_FEATURES:
            HLS_ADD_FEATURE_ESTIMATION[module] = HLS_ADD_FEATURES
          
        del HLS_ADD_FEATURES
        del HLS_ADD_FEATURES_
    
    #pp.pprint(HLS_ADD_FEATURE_ESTIMATION)

    return DEF_OP_USE, ADD_OP_DICT, DEP_G, HLS_ADD_FEATURE_ESTIMATION, HLS_CLUSTER, EdgeLabelDict, COI_DICT, OGRAPH

'''
if __name__ == "__main__":
    root_path = '/scratch/users/dp638/Work/DSP_mapping/Vivado_HLS'
    rpt_path = 'proj/sol1/.autopilot/db'
    designs = ['3_add', \
               '4_add', \
               '5_add', \
               '6_add', \
               '7_add', \
               '8_add', \
               'shared_add', \
               'bitslice_add', \
               'sine', \
               'cosine', \
               'tan', \
               'log', \
               'exp', \
               'asin'
               ]
    
    report_files = {}

    for design in designs:
        report_files[design] = get_file_names_from_loc(os.path.join(root_path, design, rpt_path), \
                '*.verbose.bind.rpt')
    
    hls_adder_chain = os.path.join('./', 'hls_adder_chain')
    mkdir(hls_adder_chain)

    for design in designs:
        design_adder_chain = os.path.join(hls_adder_chain, design)
        mkdir(design_adder_chain)

        reports = report_files[design]
        for report in reports:
            print('Analyzing report: ', report)
            comp_info, net_info = analyze(report)
            
            dest_src = {}
    
            for i in range(len(net_info)):
                dic = net_info[i]
                dest_comp = dic['dest_comp']
                src_comp = dic['src_comp']
                if dest_comp not in dest_src.keys():
                    dest_src[dest_comp] = [src_comp]
                else:
                    dest_src[dest_comp].append(src_comp)
            
            #pp.pprint(comp_info)
            #pp.pprint(net_info)
            #pp.pprint(dest_src)

            ograph, add_op_dict, op_dict, var_def, var_use, \
                    output_add = build_op_graph(comp_info, dest_src)
            dep_g = build_dep_grpah(dest_src, comp_info)
            
            ## Making a dictionary with defined variable as the key
            def_op_use = ODict()
            for key in op_dict.keys():
                def_var = op_dict[key][0]
                use_vars = op_dict[key][1]
                operation = key.split(':')
                def_op_use[def_var] = [operation[0], use_vars, int(operation[1])]

            start_idx = report.rfind('/')
            end_idx = report[start_idx + 1 :].find('.')
            module = report[start_idx + 1 : start_idx + 1 + end_idx] 

            dot_file_name = os.path.join(design_adder_chain, module + '.dot')
            out_file_name = os.path.join(design_adder_chain, module + '.pdf')

            dot_dep_file_name = os.path.join(design_adder_chain, module + '_dep.dot')
            out_dep_file_name = os.path.join(design_adder_chain, module + '_dep.pdf')

            A = to_agraph(ograph)
            A.layout()
            A.draw(dot_file_name)

            A = to_agraph(dep_g)
            A.layout()
            A.draw(dot_dep_file_name)

            dot_cmd = 'dot -Tpdf ' + dot_file_name + ' -o ' + out_file_name
            sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

            dot_cmd = 'dot -Tpdf ' + dot_dep_file_name + ' -o ' + out_dep_file_name
            sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

            if output_add:
                coi_dict = extract_coi(dep_g, output_add, var_def.keys())

            f = open(os.path.join(design_adder_chain, module + '.txt'), 'w')
            acontent = printTable2(def_op_use, ['LValue', 'Operation', 'RValue(s)', 'Comp ID'])
            f.write(acontent)
            f.write('\n' * 3)
            
            if output_add:
                f.write('HLS add operations\n\n')
                temp = {}
                for add in output_add:
                    temp[add] = [def_op_use[add][0], ', '.join(def_op_use[add][1]), def_op_use[add][2]]
                add_op_dict = ODict(sorted(temp.items(), \
                        key=lambda t: t[1][2]))
                acontent = printTable2(add_op_dict, ['LValue', 'Operation', 'Operands', 'Comp ID'])
                f.write(acontent)
                del temp
            f.close()
            
            # NOTE: Two most important data structures coming out of this analysis are
            #       i) 

            if output_add:
                del coi_dict
            del def_op_use
            del dep_g
            del output_add
            del var_use
            del var_def
            del op_dict
            del add_op_dict
            del ograph
            ## Finding the chained add outputs
            for aval in coi_dict.keys():
                deps = coi_dict[aval]
                common = list(set(deps).intersection(set(output_add)))
                if common:
                    print(aval, ' chained with: ', ', '.join(common))
'''
