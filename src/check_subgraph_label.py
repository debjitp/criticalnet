#===============================================================================
#
#         FILE: check_subgraph_label.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 08-02-2020
#     REVISION: 
#    LMODIFIED: Sat 08 Feb 2020 11:56:22 PM EST
#===============================================================================

designs = ['harris_1', 'harris_5', 'harris_6', 'gaussblur_1', 'sobel_1', 'paintmask_1']
label_loc = '/scratch/users/dp638/Work/CriticalNet/coord'
subgraph_fuse_loc = '/scratch/users/dp638/Work/CriticalNet/graph'

for design in designs:
    lfile = open(label_loc + '/' + design + '/' + design + '_gtruth.labels', 'r')
    llines = lfile.readlines()
    lfile.close()
    labels = [line.split()[-1] for line in llines]
    no_of_zero_labels = len([int(label) for label in labels if int(label) == 0])
    no_of_one_labels = len([int(label) for label in labels if int(label) == 1])

    sfile = open(subgraph_fuse_loc + '/' + design + '/subgraph/subgraph_fused.edgelist', 'r' )
    slines = sfile.readlines()
    sfile.close()
    gtruths = [line.split()[-1] for line in slines]
    no_of_zero_gtruths = len([int(gtruth) for gtruth in gtruths if int(gtruth) == 0])
    no_of_one_gtruths = len([int(gtruth) for gtruth in gtruths if int(gtruth) == 1])
    no_of_negone_gtruths = len([int(gtruth) for gtruth in gtruths if int(gtruth) == -1])

    try:
        assert(no_of_zero_labels == no_of_zero_gtruths)
        print('No of zero labels: ' + str(no_of_zero_labels) + '\n' \
              'No of zero gtruths: ' + str(no_of_zero_gtruths)
              )
        print('No of zero labels matches with no of zero gtruths for design: ' + design)
    except AssertionError:
        print('No of zero labels: ' + str(no_of_zero_labels) + '\n' \
              'No of zero gtruths: ' + str(no_of_zero_gtruths)
              )
        print("Zero label does not match for design: " + design)
    
    print('\n')
    try:
        assert(no_of_one_labels == no_of_one_gtruths)
        print('No of one labels: ' + str(no_of_one_labels) + '\n'\
              'No of one gtruths: ' + str(no_of_one_gtruths) + '\n'
              )
        print('No of one labels matches with no of one gtruths for design: ' + design)
    except AssertionError:
        print('No of one labels: ' + str(no_of_one_labels) + '\n'\
              'No of one gtruths: ' + str(no_of_one_gtruths) + '\n'
              )
        print("One label does not match for design: " + design)

    
    print('No of neg one gtruths: ' + str(no_of_negone_gtruths))
    print('#' * 10)
