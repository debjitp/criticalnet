"""
                             Define Exceptions for the Parser
                                  Yuan Zhou 03/15/2020

"""

class ParserError(Exception):
  """ Base class for parser errors """
  pass

class XMLParsingError(ParserError):
  """ Raised when parsing XMLs """
  pass
