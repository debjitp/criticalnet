#===============================================================================
#
#         FILE: adder_chain_extract.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 03-04-2020
#     REVISION: 
#    LMODIFIED: 
#===============================================================================

import os, sys
import networkx as nx
import pprint as pp
from plot_graph import make_directory as mkdir
from plot_graph import printTable2
import regex as re
from collections import OrderedDict as ODict
from itertools import combinations
from networkx.drawing.nx_agraph import *
import subprocess as sbp
from window import window


CONST0 = '<const0>'
CONST1 = '<const1>'

def adder_chain_extract(ngraph, Primitives, design, module, phase, gloc_, \
        add_op_dict, def_op_use, hls_dep_gs, hls_add_feature_estimation, hls_clusters, edgelabeldict, \
        coi_dict_add, ograph):
    
    adder_chain_head = []
    adder_chain = {}
    achain_idx = 0
   
    #name_pattern = re.compile(r'((mul_[0-9]+[a-zA-Z]s_[0-9]+[a-zA-Z]s))')
    name_pattern = re.compile(r'((?:[add]*[mul]*)+_[0-9]+[a-zA-Z]+_[0-9]+[a-zA-Z]+)')
    match = re.search(name_pattern, module)
    if match:
        print('Module ' + module + ' is a Mul/MulAdd module. Skipping for adder chain..' )
        #print('Parsing a Mul/MulAdd Module: ' + module)
        return

    try:
        CARRY8_instances = Primitives['CARRY8']
    except KeyError:
        return

    for instance in CARRY8_instances:
        inp = ngraph.nodes[instance]['inp']
        CI_connect_from = []
        for key in inp['CI'].keys():
            CI_connect_from = CI_connect_from + inp['CI'][key]
        if not list(set(CI_connect_from).intersection(set(CARRY8_instances))):
            adder_chain_head.append(instance)
    
    #pp.pprint(adder_chain_head)

    for i in range(len(adder_chain_head)):
        Queue = [adder_chain_head[i]]
        adder_chain[achain_idx] = [adder_chain_head[i]]
        while Queue:
            curr_ele = Queue.pop()
            outp = ngraph.nodes[curr_ele]['outp']
            CO_conneect_to = []
            for key in outp['CO'].keys():
                CO_conneect_to_ = outp['CO'][key]
                if not CO_conneect_to_:
                    continue
                for cell in CO_conneect_to_:
                    if 'CARRY8' in ngraph.nodes[cell]['cell_type'] and \
                            key in ngraph.nodes[cell]['inp']['CI'].keys():
                        CO_conneect_to = list(set(CO_conneect_to + [cell]))
            #ele = list(set(CO_conneect_to).intersection(set(CARRY8_instances)))
            if CO_conneect_to:
                Queue.extend(CO_conneect_to)
                adder_chain[achain_idx].extend(CO_conneect_to)

        achain_idx = achain_idx + 1
    
    del adder_chain_head
    #pp.pprint(adder_chain)
    achain_to_del = []
    for key in adder_chain.keys():
        if len(adder_chain[key]) == 1:
            achain_to_del.append(key)
    
    if achain_to_del:
        for to_del in achain_to_del:
            del adder_chain[to_del]
    
    # Summary of the adder chain clusters
    total_CARRY8 = 0

    f = open(gloc_ + '/' +  module  + '_' + phase + '.achain', 'w')
    for key in adder_chain.keys():
        adder_chain_header = 'Adder chain identifier: ' + str(key)
        f.write('>' * len(adder_chain_header) + '\n')
        f.write(adder_chain_header + '\n')
        f.write('>' * len(adder_chain_header) + '\n')
        f.write('\n')
        f.write(' ---> '.join(adder_chain[key]))
        f.write('\n\n')

        total_CARRY8 = total_CARRY8 + len(adder_chain[key])
        
    f.write('\n\n')
    f.write('Total CARRY8 block: ' + str(total_CARRY8))
    f.close()
    # Summary of the adder chain clusters

    # Details of the adder chain clusters including the input wire names, output wire names
    adder_chain_detail = adder_chain_details(ngraph, adder_chain, gloc_, module, add_op_dict, \
            def_op_use, hls_dep_gs, hls_add_feature_estimation, edgelabeldict, \
            )
    
    MATCHED = []
    SINGLE = []
    XLABELED = []
    for key in coi_dict_add.keys():
        MATCHED, MATCHED_HLS_LOGIC, MATCHED_HLS_EDGES, SINGLE, \
                XLABELED, MATCHED_ACHAIN = match_adder_chain_with_hls_add(adder_chain_detail, \
                coi_dict_add[key], ograph[key], \
                add_op_dict[key], hls_dep_gs[key], gloc_, module) 

    # Details of the adder chain clusters including the input wire names, output wire names
    '''
    if MATCHED:
        f = open(gloc_ + '/' +  module  + '_' + phase + '.cluster', 'w')
        for i in range(len(MATCHED_HLS_LOGIC)):
            if type(MATCHED_HLS_LOGIC[i]) is str:
                f.write(MATCHED_HLS_LOGIC[i])
            else:
                #f.write(', '.join(MATCHED_HLS_LOGIC[i]))
                f.write('\n'.join([k + ' ' + str('1') for k in MATCHED_HLS_LOGIC[i]]))
            if i < len(MATCHED_HLS_LOGIC) - 1:
                f.write('\n')

        if SINGLE or XLABELED:
            if SINGLE:
                f.write('\n')
                f.write('\n'.join([k + ' ' + str('0') for k in SINGLE]))
        
            if XLABELED:
                f.write('\n')
                f.write('\n'.join([k + ' x' for k in XLABELED]))
        f.close()

        f = open(gloc_ + '/' +  module  + '_' + phase + '.ecluster', 'w')
        for i in range(len(MATCHED_HLS_EDGES)):
            if type(MATCHED_HLS_EDGES[i]) is str:
                f.write(MATCHED_HLS_EDGES[i])
            else:
                #f.write(', '.join(MATCHED_HLS_LOGIC[i]))
                f.write('\n'.join([k[0] + ' ' + k[1] for k in MATCHED_HLS_EDGES[i]]))
            if i < len(MATCHED_HLS_EDGES) - 1:
                f.write('\n')
        f.close()

        f = open(gloc_ + '/' +  module  + '_' + phase + '.acluster', 'w')
        for i in range(len(MATCHED)):
            if type(MATCHED[i]) is str:
                f.write(MATCHED[i])
            else:
                f.write(', '.join(MATCHED[i]))
            if i < len(MATCHED) - 1:
                f.write('\n')
        f.close()
    '''
    # Change needed to dump all the add-clusters in the same file when parsing the addmul modules
    # It is a hack. Needs a proper fix.
    if MATCHED:
        f = open(gloc_ + '/' +  design  + '_' + phase + '.cluster', 'a+')
        for i in range(len(MATCHED_HLS_LOGIC)):
            if type(MATCHED_HLS_LOGIC[i]) is str:
                f.write(MATCHED_HLS_LOGIC[i])
            else:
                #f.write(', '.join(MATCHED_HLS_LOGIC[i]))
                f.write('\n'.join([k + ' ' + str('1') for k in MATCHED_HLS_LOGIC[i]]))
            if i < len(MATCHED_HLS_LOGIC) - 1:
                f.write('\n')

        if SINGLE or XLABELED:
            if SINGLE:
                f.write('\n')
                f.write('\n'.join([k + ' ' + str('0') for k in SINGLE]))
        
            if XLABELED:
                f.write('\n')
                f.write('\n'.join([k + ' x' for k in XLABELED]))
        f.write('\n')
        f.close()

        f = open(gloc_ + '/' +  design  + '_' + phase + '.ecluster', 'a+')
        for i in range(len(MATCHED_HLS_EDGES)):
            if type(MATCHED_HLS_EDGES[i]) is str:
                f.write(MATCHED_HLS_EDGES[i])
            else:
                #f.write(', '.join(MATCHED_HLS_LOGIC[i]))
                f.write('\n'.join([k[0] + ' ' + k[1] for k in MATCHED_HLS_EDGES[i]]))
            if i < len(MATCHED_HLS_EDGES) - 1:
                f.write('\n')
        f.write('\n')
        f.close()

        f = open(gloc_ + '/' +  design  + '_' + phase + '.acluster', 'a+')
        for i in range(len(MATCHED)):
            if type(MATCHED[i]) is str:
                f.write(MATCHED[i])
            else:
                f.write(', '.join(MATCHED[i]))
            if i < len(MATCHED) - 1:
                f.write('\n')
        f.write('\n')
        f.close()

        f = open(gloc_ + '/' +  design  + '_' + phase + '.accluster', 'a+')
        if MATCHED_ACHAIN:
            f.write('\n'.join([str(i) for i in MATCHED_ACHAIN]))
        f.write('\n')
        f.close()

    try:
        hls_add_cluster = hls_clusters[module]
        if hls_add_cluster:
            f = open(gloc_ + '/' + module + '.hcluster', 'w')
            for i in range(len(hls_add_cluster)):
                f.write(', '.join(hls_add_cluster[i]))
                if i < len(hls_add_cluster) - 1:
                    f.write('\n')
            f.close()
    except KeyError:
        pass

    return


def is_hport_sport_match(port, values):
    for value in values:
        # For synthetic designs using CD code
        if value == 'funct_output' and 'ap_return' in port:
            return True, value
        # For synthetic designs using CD code

        # NOTE: Special case: HLS primary input variable name length is greater than
        #       the primary input of the synthesized netlist
        '''
        if len(value) > len(port):
            try:
                index = value.index(port)
                print('match')
            except ValueError:
                continue

            if index == 0:
                try:
                    assert(value[len(port)] == '_')
                    return True, value
                except AssertionError:
                    return False, ''
        '''
        try:
            index = value.index('_read')
            value_ = value[:index]
        except ValueError:
            value_ = value

        try:
            index = port.index(value_)
        except ValueError:
            continue
        #if value in port:
        if index == 0:
            return True, value

    return False, ''

def feasible_adds(hls_add_combinations, ograph, cindex):

    feasible = []

    for add in hls_add_combinations:
        print('Add: ', add)
        found = True
        for i in range(len(add) - 1):
            if not nx.has_path(ograph, add[i], add[-1]):
                found = False
                break
            else:
                paths = nx.all_simple_paths(ograph, source=add[i], target=add[-1])
                for path in paths:
                    print(path, cindex)
                    #if len(path) <= 2:
                    if len(path) <= cindex:
                        continue
                    pathOk= True
                    #for node in path:
                    no_of_add_sub = 0
                    for nindex in range(1, len(path) - 1):
                        if 'add' in path[nindex] or 'sub' in path[nindex]:
                            # NOTE: If the number of add sub in the path is less than cindex -2 
                            #       2 appears for the first and the last add/sub. cindex is the total
                            #       number of add/sub that are supposed to be in the cluster
                            no_of_add_sub = no_of_add_sub + 1
                            if no_of_add_sub <= cindex - 2:
                                continue
                            else:
                                pathOk = False
                                break
                    if not pathOk:
                        found = False
                        break
        if found:
            feasible.append(add)
    
    return feasible

def match_1(inputs_acd, inputs_coi):
    
    print('$' * 10)
    print(inputs_acd)
    print(inputs_coi)

    inputs_acd_ = inputs_acd[:]
    inputs_coi_ = inputs_coi[:]
    while inputs_acd_:
        inp =inputs_acd_.pop(0)
        if inp == 'ap_return' or inp == CONST0 or inp == CONST1 or inp == 'b_d0':
            continue
        stat, value = is_hport_sport_match(inp, inputs_coi_)
        print('Netlist input %s is matched to HLS input #%s#.' % (inp, value))
        if not stat:
            return False
        inputs_coi_ = list(set(inputs_coi_).difference(set([value])))
    
    print('#' * 10)
    return True

def match_2(inputs_acd, inputs_coi):
    
    print('$' * 10)
    print(inputs_acd)
    print(inputs_coi)

    inputs_acd_ = sorted(inputs_acd['DI'])[:]
    inputs_coi_ = inputs_coi[:]

    hls_signals = []

    while inputs_acd_:
        inp =inputs_acd_.pop(0)
        if inp == 'ap_return' or inp == CONST0 or inp == CONST1 or inp == 'b_d0':
            continue
        stat, value = is_hport_sport_match(inp, inputs_coi_)
        print('Netlist input %s is matched to HLS input #%s#.' % (inp, value))
        if not stat:
            return False, hls_signals
        else:
            hls_signals.append(value)
        inputs_coi_ = list(set(inputs_coi_).difference(set([value])))
    

    inputs_acd_ = sorted(inputs_acd['S'])[:]
    while inputs_acd_:
        inp =inputs_acd_.pop(0)
        if inp == 'ap_return' or inp == CONST0 or inp == CONST1 or inp == 'b_d0':
            continue
        stat, value = is_hport_sport_match(inp, inputs_coi_)
        print('Netlist input %s is matched to HLS input #%s#.' % (inp, value))
        if not stat:
            return False, hls_signals
        else:
            hls_signals.append(value)
        inputs_coi_ = list(set(inputs_coi_).difference(set([value])))

    print('#' * 10)
    return True, hls_signals

def match_adder_chain_with_hls_add(adder_chain_detail, coi_dict, ograph, add_op_dict, \
        hls_dep_g, gloc_, module):
    MATCHED = []
    MATCHED_ACHAIN = []
    MATCHED_HLS_LOGIC = []
    MATCHED_HLS_EDGES = []
    matched_hls = []
    matched_achain = []

    SINGLE = []
    XLABELED = []

    flatten = lambda l: [item for sublist in l for item in sublist]
    #pp.pprint(adder_chain_detail)
    #pp.pprint(coi_dict)

    for key_ in coi_dict.keys():
        if key_ not in matched_hls:
            inputs_coi = coi_dict[key_]
        else:
            continue
        for key in adder_chain_detail.keys():
            if key not in matched_achain:
                inputs_acd = adder_chain_detail[key]
            else:
                continue
            print('Working on pair (%s, %s).' % (key_, key))
            #pp.pprint(inputs_acd)
            # NOTE: Implement match
            stat, _ = match_2(inputs_acd, sorted(inputs_coi))
            if stat:
                matched_hls.append(key_)
                matched_achain.append(key)
                break
    
    print('\n' * 2)
    
    if matched_hls:
        #MATCHED = MATCHED + matched_hls
        SINGLE = matched_hls
        print('Single Add Operations Matched: %s' % (', '.join(matched_hls)))

    unmatched_hls = sorted(list(set(coi_dict.keys()).difference(set(matched_hls))), key=lambda t:int(t[3:]))
    unmatched_achain = list(set([i for i in adder_chain_detail.keys()]).difference(set(matched_achain)))
    
    print('Single add/sub operations matched following chains: %s' % \
            (', '.join([str(i) for i in matched_achain])))

    for mhls in matched_hls:
        ograph.remove_node(mhls)

    del matched_hls
    del matched_achain

    design_adder_chain = os.path.join(gloc_, 'hls_adder_chain')
    dot_file_name = os.path.join(design_adder_chain, module + '_post_single_match.dot')
    out_file_name = os.path.join(design_adder_chain, module + '_post_single_match.pdf')
    
    A = to_agraph(ograph)
    A.layout()
    A.draw(dot_file_name)
 
    dot_cmd = 'dot -Tpdf ' + dot_file_name + ' -o ' + out_file_name
    sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)

    print('\n' * 2)
    print('HLS adds still needs to be matched: %s.' % (', '.join(unmatched_hls)))
    print('Adder chain still needs to be matched: %s.\n\n' % (', '.join([str(i) for i in unmatched_achain])))

    cindex = 2
    hls_add_combinations = []
    #if unmatched_hls:
        # Sliding window to make sure we are clustering consecutive HLS operations
        # Sliding window min = 2
        # Sliding window max = len(unmatched_hls)
        #for i in range(2, len(unmatched_hls) + 1):
            #hls_add_combinations.extend([j for j in window(unmatched_hls, i)])
            #hls_add_combinations.extend([j for j in combinations(unmatched_hls, i)])
    
    #hls_add_combinations.extend([j for j in combinations(unmatched_hls, 2)])
    hls_add_combinations = [j for j in combinations(unmatched_hls, cindex)]
    hls_add_combinations = feasible_adds(hls_add_combinations, ograph, cindex)

    unmatched_prev_hls = unmatched_hls[:]
    iter = 0
    
    ograph_label = {}
    for edge in ograph.edges():
        try:
            ograph_label[ograph.edges[edge[0], edge[1]]['label']] = edge[0]
        except KeyError:
            pass

    while hls_add_combinations:
        print('Feasible add combinations:')
        pp.pprint(hls_add_combinations)

        
        matched_hls = []
        matched_hls_logic = []
        matched_hls_edges = []
        matched_achain = []

        for i in hls_add_combinations:
            inputs_coi = []
            if i not in matched_hls:
                for j in i:
                    inputs_coi = inputs_coi + coi_dict[j]
            else:
                continue
            for uachain in unmatched_achain:
                if uachain not in matched_achain:
                    inputs_acd = adder_chain_detail[uachain]
                else:
                    continue
                print('Working on pair (%s, %s).' % (str(i), uachain))
                stat, hls_signals = match_2(inputs_acd, sorted(inputs_coi))
                if stat:
                    #hls_signals = hls_signals + [key if add_op_dict[key][2] == i[-1][3:] \
                    #       for key in add_op_dict.keys()]
                    matched_hls.append(i)
                    hls_signals, hls_edges = traverse_vdg(hls_signals, i, ograph_label, ograph)
                    matched_hls_logic.append(hls_signals)
                    matched_hls_edges.append(hls_edges)
                    matched_achain.append(uachain)
                    break

        print('\n' * 2)
        if matched_hls:
            MATCHED = MATCHED + matched_hls
            MATCHED_ACHAIN = MATCHED_ACHAIN + matched_achain
            MATCHED_HLS_LOGIC = MATCHED_HLS_LOGIC + matched_hls_logic
            MATCHED_HLS_EDGES = MATCHED_HLS_EDGES + matched_hls_edges
            print('Clustered add found %s.' % (', '.join([str(i) for i in matched_hls])))
            print('Clustered add found %s.' % (', '.join([str(i) for i in matched_hls_logic])))
            print('Clustered edge found %s.' % (', '.join([str(i) for i in matched_hls_edges])))
    
            print('Clustered add/sub operations matched following chains: %s' % \
                    (', '.join([str(i) for i in matched_achain])))

        
        unmatched_hls = list(set(unmatched_hls).difference(set(flatten(matched_hls))))
        unmatched_achain = list(set(unmatched_achain).difference(set(matched_achain)))

        unmatched_hls = sorted(unmatched_hls, key=lambda t: int(t[3:]))

        if len(unmatched_prev_hls) > len(unmatched_hls):
            unmatched_prev_hls = unmatched_hls[:]
        else:
            iter = iter + 1

        if iter >= 3:
            XLABELED = unmatched_hls[:]
            del matched_hls
            del matched_achain
            break

        if unmatched_hls:
            print(unmatched_hls)
            cindex = cindex + 1
            hls_add_combinations = [j for j in combinations(unmatched_hls, cindex)]
            hls_add_combinations = feasible_adds(hls_add_combinations, ograph, cindex)
            if not hls_add_combinations:
                XLABELED = unmatched_hls[:]

        del matched_hls
        del matched_achain

    return MATCHED, MATCHED_HLS_LOGIC, MATCHED_HLS_EDGES, SINGLE, XLABELED, MATCHED_ACHAIN

def traverse_vdg(hls_signals, i, ograph_label, ograph):
    dest_node = i[-1]
    flatten = lambda l: [item for sublist in l for item in sublist]
    PATH = []
    EDGE = []
    for hls_signal in hls_signals:
        try:
            src_node = ograph_label[hls_signal]
        except KeyError:
            continue
        paths = nx.all_simple_paths(ograph, source=src_node, target=dest_node)
        for path in paths:
            if set(path[:-1]).intersection(set(i[:-1])):
                PATH.append([i for i in path if 'read' not in i])
    
    for i in PATH:
        win = window(i, 2)
        for j in win:
            EDGE.append(tuple([j[0], j[1]]))
    
    #print(list(set(EDGE)))

    return list(set(flatten(PATH))), list(set(EDGE))

def adder_chain_details(ngraph, adder_chain, gloc_, module, add_op_dict, \
        def_op_use, hls_dep_gs, hls_add_feature_estimation, edgelabeldict):
    

    flatten = lambda l: [item for sublist in l for item in sublist]

    adder_chain_detail = {}

    achain_dir = gloc_ + '/adder_chain'
    mkdir(achain_dir)
    strings = []
    for key_ in adder_chain.keys():
        keystring = str(key_)
        inputs_DI = {}
        inputs_S = {}
        outputs_O = {}
    
        inputs_all_DI = []
        inputs_all_S = []
        outputs_all = []

        CARRY8s = adder_chain[key_]
        # These are meant for checking the non-adder carry chains
        DI_inputs = []
        CI_inputs = []
        S_inputs = []

        LUT_counts = 0
        LUT_names = []
        #for CARRY8 in CARRY8s:
        for j in range(len(CARRY8s)):
            inp = ngraph.nodes[CARRY8s[j]]['inp']
            outp = ngraph.nodes[CARRY8s[j]]['outp']
            
            for key in inp.keys():
                if key == 'DI':
                    #print('CARRY8 name: ' + CARRY8s[j], key)
                    inputs, LUT_count, LUT_name = analyze_DI_CI_S(ngraph, inp['DI'])
                    if not inputs:
                        inputs = list(set([key[:key.index('_fu')] if '_fu' in key else key \
                                for key in bit_to_word_ports(inp['DI'].keys())]))
                    inputs_DI[CARRY8s[j]] = ', '.join(inputs)
                    DI_inputs.append(inputs)
                    LUT_counts = LUT_counts + LUT_count
                    LUT_names = LUT_names + LUT_name
                    #inputs_all = inputs_all + inputs
                elif key == 'CI':
                    #print('CARRY8 name: ' + CARRY8s[j], key)
                    inputs, LUT_count, LUT_name = analyze_DI_CI_S(ngraph, inp['CI'])
                    CI_inputs.append(inputs)
                    LUT_counts = LUT_counts + LUT_count
                    LUT_names = LUT_names + LUT_name
                elif key == 'S':
                    #print('CARRY8 name: ' + CARRY8s[j], key)
                    inputs, LUT_count, LUT_name = analyze_DI_CI_S(ngraph, inp['S'])
                    inputs_S[CARRY8s[j]] = ', '.join(inputs)
                    S_inputs.append(inputs)
                    LUT_counts = LUT_counts + LUT_count
                    LUT_names = LUT_names + LUT_name
                else:
                    continue

            for key in outp.keys():
                if key == 'O':
                    #outputs = analyze_O(ngraph, outp['O'])
                    outputs = outp['O'].keys()
                    outputs_O[CARRY8s[j]] = ', '.join(outputs)
                    outputs_all = outputs_all + [i for i in outputs]
                else:
                    continue
        

        f = open(achain_dir + '/' + module + '_' + keystring + '.details', 'w')
        
        icontent = printTable2(inputs_DI, ['CARRY8 module', 'DI Inputs'])
        f.write(icontent)
        f.write('\n' * 4)
        icontent = printTable2(inputs_S, ['CARRY8 module', 'S Inputs'])
        f.write(icontent)
        f.write('\n' * 4)
        ocontent = printTable2(outputs_O, ['CARRY8 module', 'CO Outputs'])
        f.write(ocontent)


        del inputs_DI
        del outputs_O
        
        # Not doing further adder chain matching if we can already identify it is 
        # not an ADDER CHAIN rather some sort of logic operations like truncation/select/partselect)
        is_not_adder_chain = is_adder_chain(DI_inputs, CI_inputs)

        if is_not_adder_chain:
            continue

        inputs_all_DI = flatten(DI_inputs)
        inputs_all_S = flatten(S_inputs)
        
        #adder_chain_detail[key_] = list(set(inputs_all_DI + inputs_all_S))
        adder_chain_detail[key_] = {'DI': list(set(inputs_all_DI)),
                                    'S' : list(set(inputs_all_S).difference(set(inputs_all_DI)))
                                    }

        f.write('\n' * 4)
        f.write('Input to DI: %s' % (', '.join(sorted(list(set(inputs_all_DI))))))
        f.write('\n')
        f.write('Input exclusive to S: %s' % (', '.join(sorted(list(set(inputs_all_S).difference(set(inputs_all_DI)))))))
        f.write('\n')
        f.write('Number of the LUTs feeding to CARRY8 chain: %d' % (len(LUT_names)))
        f.write('\n')
        f.write('Name of the LUTs feeding to CARRY8 chain: \n%s' % ('\n'.join(LUT_names)))
        f.close()

        del LUT_names
        ''' 
        add_string, lut_string, bit_string = analyze_inputs_outputs(module, \
                list(set(inputs_all_DI + inputs_all_S)), \
                list(set(outputs_all)), \
                add_op_dict, def_op_use, \
                hls_dep_gs, \
                hls_add_feature_estimation, \
                edgelabeldict)

        if add_string:
            print('ADDER_DATA: Add: %s, HLS LUT counts: %s, HLS bit: %s, Synth LUT: %d, Synth CARRY8s: %d' % (add_string, lut_string, bit_string, LUT_counts, len(CARRY8s)))
            strings.append('\t'.join([add_string, lut_string, bit_string, str(LUT_counts), str(len(CARRY8s))]))
        '''
    adder_chain_detail = ODict(sorted(adder_chain_detail.items(), key=lambda t: len(t[1])))

    return adder_chain_detail

def is_select(DI_inputs, CI_inputs):
    
    flatten = lambda l: [item for sublist in l for item in sublist]
    CI0_grounded = False
    DI_inputs_grounded = False

    CI0 = list(set(CI_inputs[0]))
    DI_inputs_ = list(set(flatten(DI_inputs[1:])))

    if len(CI0) == 1 and CONST0 in CI0[0]:
        CI0_grounded = True
    if len(DI_inputs_) and CONST0 in DI_inputs_:
        DI_inputs_grounded = True

    return CI0_grounded and DI_inputs_grounded
        
def is_trunc(DI_inputs, CI_inputs):

    flatten = lambda l: [item for sublist in l for item in sublist]
    CI0_grounded = False
    DI_inputs_grounded = False
    DI0_gnd_vcc = False

    CI0 = list(set(CI_inputs[0]))
    DI_inputs_ = list(set(flatten(DI_inputs[1:])))
    DI0 = list(set(DI_inputs[0]))

    if len(CI0) == 1 and CONST0 in CI0[0]:
        CI0_grounded = True
    if len(DI_inputs_) and CONST0 in DI_inputs_:
        DI_inputs_grounded = True
    if len(DI0) == 2 and CONST0 in DI0 and CONST1 in DI0:
        DI0_gnd_vcc = True

    return CI0_grounded and DI_inputs_grounded and DI0_gnd_vcc

def is_adder_chain(DI_inputs, CI_inputs):

    select = is_select(DI_inputs, CI_inputs)
    if select:
        return True
    trunc = is_trunc(DI_inputs, CI_inputs)
    if trunc:
        return True

    return False


def analyze_inputs_outputs(module, inputs_DI, outputs, add_op_dict, def_op_use, \
        hls_dep_gs, hls_add_feature_estimation, edgelabeldict):

    EMPTY = ''
    hls_add_clustered = {}
    hls_inputs = []
    # Key is the HLS signal name and the value is the Netlist signal name
    inp_outp_map = {}
    hls_op_nodes = []

    if 'ap_return' in inputs_DI:
        inputs_DI.remove('ap_return')

    if not inputs_DI:
        print('No input to input ports found. Returning..\n\n')
        print('\n' * 2)
        print('--' * 10)
        return EMPTY, EMPTY, EMPTY

    outputs = bit_to_word_ports(outputs)
    
    print('\n' * 2)
    print('Name of the module being analyzed: ', module)
    print('Analyzing netlist output: ', ', '.join(outputs))
    print('\n' * 2)
    
    # Assumption: output(s) has/have to match with the lvalue of the one of the add
    #             sub operation in the HLS
    for outp in outputs:
        # add_op_dict: Is the dictionary of the add operations of all modules
        for key in add_op_dict.keys():
            # op_dict: Is the dictionary of the add operation for a single module
            #print('Key: ', key)
            hls_dep_g_ = hls_dep_gs[key]
            op_dict = add_op_dict[key]
            #pp.pprint(hls_add_feature_estimation[key])
            for key_ in op_dict.keys():
                #print('Key_: ', key_)
                stat, lvalue = is_hport_sport_match(outp, [key_])
                if not stat:
                    add_output_bfs = BFS_f(hls_dep_g_, [key_], 1)
                    stat, lvalue = is_hport_sport_match(outp, add_output_bfs)
                if stat:
                    #inp_outp_map = {'hls': lvalue, 'netlist': outp, 'type': 'O'}
                    #hls_inputs = op_dict[lvalue][1].split(',')
                    #hls_inputs.extend(op_dict[key_][1].split(','))
                    hls_dep_g = hls_dep_g_
                    hls_inputs.extend(BFS_b(hls_dep_g_, [lvalue], 1))
                    hls_op_nodes.extend(op_dict[key_][1].split(','))
                    hls_add_clustered[lvalue] = op_dict[key_]
                    print("Step 0: Netlist output %s is matched to HLS output %s" % (outp, lvalue))
                    #print(hls_add_feature_estimation[key][key_])
                    #print(edgelabeldict[key][lvalue][0])

                    hls_add_feature_estimation_ = hls_add_feature_estimation[key]
                    #pp.pprint(hls_add_feature_estimation_)
                    edgelabeldict_ = edgelabeldict[key]
                    op_dict_ = op_dict
                    for ele in add_output_bfs:
                        inp_outp_map[ele] = {'type': 'O'}
                        #hls_op_nodes.append(edgelabeldict[key][ele][0])

    # If the first assumption is not met return. Don't know where to start
    if not inp_outp_map:
        print('Step 0: Unable to match netlist outputs with HLS outputs. Returning')
        print('\n' * 2)
        print('--' * 10)
        return EMPTY, EMPTY, EMPTY

    print('Step 0: Inputs for netlist: ' + ', '.join(inputs_DI))
    print('Step 0: Inputs for HLS: ' + ', '.join(hls_inputs))
    print('\n' * 2) 

    # First LUT level input to the CARRY 8 that matches with HLS imemdiately
    inputs_DI_ = []
    # Right hand operands of the HLS add operations that matches with netlist LUT inputs
    # immediately
    hls_inputs_ = []
    for val in hls_inputs:
        inp_outp_map[val] = {'type': 'I'}
    for inp in inputs_DI:
        stat, value = is_hport_sport_match(inp, hls_inputs)
        if stat:
            inputs_DI_.append(inp)
            hls_inputs_.append(value)
            hls_op_nodes.append(value)
            #inp_outp_map = {'hls': value, 'netlist': inp, 'type': 'I'}
            inp_outp_map[value] = {'type': 'I'}
            print("Step 1: Netlist input %s is matched to HLS input %s" % (inp, value))
            print("Step 1: Corresponding ops: %s" % (', '.join(edgelabeldict_[value])))

    # NOTE: Start from here. How to systematically explore the inputs of the LUTs preceding 
    #       the CARRY8
    # Remaining netlist inputs that need to be matched with HLS inputs
    inputs_DI = list(set(inputs_DI).difference(set(inputs_DI_)))
    hls_inputs = list(set(hls_inputs).difference(set(hls_inputs_)))
    
    print('Step 1: Inputs still needs to be matched: ' + ', '.join(inputs_DI))
    print('Step 1: HLS inputs still needs to be matched: ' + ', '.join(hls_inputs))
    
    # Checking if the inputs are already matched and no need for further exploration
    if not inputs_DI:
        print('Voila: ADD MATCHED!!')
        print('\n' * 2)
        print('--' * 10)
        # FIXME: Bring the code for add analysis
        return EMPTY, EMPTY, EMPTY
    elif not hls_inputs:
        #print('Voila: HLS ADD MATCHED')
        print('\n' * 2)
        print('--' * 10)
        return EMPTY, EMPTY, EMPTY

    print('\n' * 2)
    
    del inputs_DI_
    del hls_inputs_

    # Start the lock-step BFS (backwards) search
    rnum = 0
    while inputs_DI:
        # Inputs that are matched in this iteration. Store them for removal from further
        # consideration
        prev_inputs = inputs_DI[:]
        inputs_DI_ = []
        # HLS inputs that got matched in this iteration. Store them for removal from further
        # consideration
        hls_inputs_ = []
        hls_inputs__ = BFS_b(hls_dep_g, hls_inputs, 1) # Was 3. Checking with 1
        for val in hls_inputs__:
            inp_outp_map[val] = {'type': 'I'}

        print('Step 2: After BFS (backwards) HLS inputs__: ' + ', '.join(hls_inputs__))
        for inp in inputs_DI:
            stat, value = is_hport_sport_match(inp, hls_inputs__)
            #print('Input DI being considered: %s.\n' % (inp))
            if stat:
                inputs_DI_.append(inp)
                hls_inputs_.append(value)
                #hls_inputs__.remove(value)
                hls_op_nodes.append(value)
                #inp_outp_map = {'hls': value, 'netlist': inp, 'type': 'I'}
                #inp_outp_map[value] = {'type': 'I'}
                print("Netlist input %s is matched to HLS input %s" % (inp, value))
                #print("Corresponding ops: %s" % (', '.join(edgelabeldict_[value])))

        inputs_DI = list(set(inputs_DI).difference(set(inputs_DI_)))
        hls_inputs = list(set(hls_inputs__).difference(set(hls_inputs_)))
        #del hls_inputs__
        #del hls_inputs_
        hls_inputs = hls_inputs__
        if inputs_DI:
            print('Input still remaining: ' + ', '.join(inputs_DI))
        else:
            #pp.pprint(inp_outp_map)
            #adds = list(set(inp_outp_map.keys()).intersection(set(op_dict_.keys())))
            adds = []
            lvals = []
            for key in op_dict_.keys():
                if list(set(hls_op_nodes).intersection(set(op_dict[key][1].split(',')))):
                    adds.append('add' + str(op_dict_[key][2]))
                    lvals.append(key)
            #print(adds)
            add_string = []
            bit_string = []
            lut_string = []
            for add in lvals:
                #add_string.append(op_dict_[add][0] + str(op_dict_[add][2]))
                ## For the synthetic dataset
                bit_string.append(hls_add_feature_estimation_[add]['bitP0'])
                lut_string.append(hls_add_feature_estimation_[add]['LUT'])
                ## For the synthetic dataset
            print(list(set(hls_op_nodes)))
            print('Voila: ADD MATCHED!!')
            return ', '.join(adds), ', '.join(str(i) for i in lut_string), \
                    ', '.join(str(i) for i in bit_string)
    
        if len(list(set(prev_inputs).intersection(set(inputs_DI)))) == len(prev_inputs):
            rnum = rnum + 1
        if rnum >= 10:
            return EMPTY, EMPTY, EMPTY

    print('\n' * 2)
    print('--' * 10)

    return EMPTY, EMPTY, EMPTY

def BFS_b(graph, snodes, depth):
    '''
    graph: A directed HLS variable dependency graph
    snodes: A list of the Rvalue nodes in the HLS variable dependency graph 
    depth: Depth that needs to be visited for the HLS vaariable dependency graph in the backward direction
    '''
    op_pattern = re.compile(r'([a-zA-Z]+)([0-9]+)')
    
    Q = []
    visited = snodes[:]
    add_traversed = []
    cdepth = 0
    for snode in snodes:
        graph.nodes[snode]['visited'] = True
    Q.append(snodes)
    while Q:
        if cdepth >= depth:
            break
        cnodes = Q.pop(0)
        Q_ = []
        for cnode in cnodes:
            for nnode in graph.predecessors(cnode):
                # NOTE: The following step will prevent to go to the previous input if the current variable
                #       is a result of the mul operation.
                #       for example, add_ln31_2 = add a18, sext_ln31_1
                #                    a18 = mul a1_read, add_ln29
                #       The def_by will prevent to do a backward BFS and reach add_ln29 via a18.
                #       This will effectively ensure that we are not leapfrogging any mul operation as 
                #       a part of the adder chain
                match = re.search(op_pattern, graph.edges[nnode, cnode]['u_by'])
                used_by = [match.group(1)]
                if not graph.nodes[nnode]['visited'] and 'mul' not in used_by:
                    add_traversed.append(graph.edges[nnode, cnode]['d_by'])
                    visited.append(nnode)
                    Q_.append(nnode)
                    graph.nodes[nnode]['visited'] = True
        Q.append(Q_)
        del Q_
        cdepth = cdepth + 1

    for node in visited:
        graph.nodes[node]['visited'] = False
    
    del Q
    
    return list(set(visited).difference(set(snodes)))

def BFS_f(graph, snodes, depth):
    '''
    graph: A directed HLS variable dependency graph
    snodes: A list of the Lvalue node in the HLS variable dependency graph 
    depth: Depth that needs to be visited for the HLS variable dependency graph in the forward direction
    '''
    op_pattern = re.compile(r'([a-zA-Z]+)([0-9]+)')

    Q = []
    visited = snodes[:]
    cdepth = 0
    for snode in snodes:
        graph.nodes[snode]['visited'] = True
    Q.append(snodes)
    #print(Q)
    while Q:
        if cdepth >= depth:
            break
        cnodes = Q.pop(0)
        Q_ = []
        for cnode in cnodes:
            for nnode in graph.successors(cnode):
                # NOTE: The following step will prevent to go to the next add output of a chained
                #       add operations. 
                #       For example, add_ln31 = add a1, trunc_ln23
                #                    funct_output = add add_ln31, add_ln31_2
                #       The used_by will prevent to do a forward BFS and reach funct_output erroneously.
                #       This will effectively enforce that only proper add outputs are matched
                match = re.search(op_pattern, graph.edges[cnode, nnode]['u_by'])
                used_by = [match.group(1)]
                if not graph.nodes[nnode]['visited'] and 'add' not in used_by and 'mul' not in used_by:
                    visited.append(nnode)
                    Q_.append(nnode)
                    graph.nodes[nnode]['visited'] = True
        Q.append(Q_)
        del Q_
        cdepth = cdepth + 1

    for node in visited:
        graph.nodes[node]['visited'] = False

    del Q

    return visited

def analyze_O(ngraph, O):
    flatten = lambda l: [item for sublist in l for item in sublist]
    
    O_connects_to = []
    for key in O.keys():
        O_connects_to = O_connects_to + O[key]
    
    outputs = []

    for ele in O_connects_to:
        #print(ngraph.nodes[ele]['cell_type'])
        if 'LUT' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                #outputs = outputs + flatten([outp[key][key1] for key1 in outp[key].keys()])
                outputs = outputs + [key1 for key1 in outp[key].keys()]
        elif 'OBUF' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                outputs = outputs + [key1 for key1 in outp[key].keys()]
        elif 'FDRE' in ngraph.nodes[ele]['cell_type'] or 'FDSE' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                outputs = outputs + [key1 for key1 in outp[key].keys()]
        elif 'DSP48E2' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                if key == 'P':
                    outputs = outputs + [key1 for key1 in outp[key].keys()]

    outputs = list(set(bit_to_word_ports(outputs)))
    return outputs

def analyze_DI_CI_S(ngraph, DI):
    flatten = lambda l: [item for sublist in l for item in sublist]
    DI_connects_from = []
    for key in DI.keys():
        DI_connects_from = DI_connects_from + DI[key]
    
    inputs = []

    LUT_count = 0
    LUT_name = []

    while DI_connects_from:
        #print(DI_connects_from)
        ele = DI_connects_from.pop(0)
        #print(ngraph.nodes[ele]['cell_type'])
        if 'LUT' in ngraph.nodes[ele]['cell_type']:
            LUT_count = LUT_count + 1
            LUT_name.append(ele)
            inp = ngraph.nodes[ele]['inp']
            for key in inp.keys():
                inputs = inputs + [key1[:key1.index('_fu')] if '_fu' in key1 else key1 \
                        for key1 in inp[key].keys()]
                for key1 in inp[key].keys():
                    if not inp[key][key1]:
                        continue
                    if 'LUT' in ngraph.nodes[inp[key][key1][0]]['cell_type']:
                        DI_connects_from = DI_connects_from + inp[key][key1]
        elif 'IBUF' in ngraph.nodes[ele]['cell_type']:
            inp = ngraph.nodes[ele]['inp']
            for key in inp.keys():
                inputs = inputs + [key1 for key1 in inp[key].keys()]
        elif 'FDRE' in ngraph.nodes[ele]['cell_type'] or 'FDSE' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                if key == 'Q':
                    inputs = inputs + [key1 for key1 in outp[key].keys()]
        elif 'DSP48E2' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                if key == 'P':
                    inputs = inputs + [key1[:key1.index('_fu')] if '_fu' in key1 else key1 \
                            for key1 in outp[key].keys()]
        elif 'CARRY8' in ngraph.nodes[ele]['cell_type']:
            outp = ngraph.nodes[ele]['outp']
            for key in outp.keys():
                if key == 'O':
                    #inputs = inputs + [key1 for key1 in outp[key].keys()]
                    inputs = inputs + [key1[:key1.index('_fu')] if '_fu' in key1 else key1 \
                            for key1 in outp[key].keys()]


        # FIXME
        # NOTE: Special cases:
        #       1) Input is coming from a previous CARRY8 block
        #       2) Input is a primary input
        #else:
        #    inputs = 

    inputs = list(set(bit_to_word_ports(inputs)))
    return inputs, LUT_count, LUT_name

def bit_to_word_ports(ports):
    port_ = {}
    for ports__ in ports:
        prefix = ports__[:ports__.rfind('/')] if ports__.rfind('/') != -1 else ''
        ports_ = ports__[ports__.rfind('/') + 1:] if ports__.rfind('/') != -1 else ports__
        iname = ports_[:ports_.find('[')] if ports_.find('[') != -1 else ports_
        bit = ports_[ports_.find('[') + 1 : ports_.find(']')] if ports_.find('[') != -1 else 0
        if iname not in port_.keys():
            port_[iname] = [int(bit)]
        else:
            port_[iname].append(int(bit))

    return port_.keys()
