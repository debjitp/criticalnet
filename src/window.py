######################################################################
# Copied from: https://coderwall.com/p/zvuvmg/sliding-window-in-python
######################################################################

def window(iterable, size=3):
    i = iter(iterable)
    win = []
    for e in range(0, size):
        win.append(next(i))

    yield win
    for e in i:
        win = win[1:] + [e]
        yield win

#iterable = 'sub94,add100,add142,add148,sub184,add190,add196,add210,add216,add226,sub232'.split(',')
#print([i for i in window(iterable, 2)])
