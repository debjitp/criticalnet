#===============================================================================
#
#         FILE: netlist_to_graph.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 14-01-2020
#     REVISION: 
#    LMODIFIED: Thu 20 Feb 2020 01:01:53 PM EST
#===============================================================================

import os, sys
from pyverilog.vparser.parser import parse
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
from logic import logics_xcvu440_flga2892_2_e as lxcvu440
from plot_graph import plot_graph as pg
from plot_graph import make_directory as mkdir
from plot_graph import fill_color
from plot_graph import printTable3
from extract_subgraph import extract_subgraph as exsg
#from extract_hls_graph import extract_hls_graph as exhg
from extract_hls_graph_realistic import extract_hls_graph as exhg
from adder_chain_extract import adder_chain_extract as acex
from visualize import visualize

import networkx as nx
import pprint as pp


def main(files, gloc, lloc, cloc, gnd, vcc, phase, design):
    
    ast, directives = parse_verilog([files], [], [])

    ModuleDefs = {}
    ModuleInstances = {}
    get_modules(ast, ModuleDefs, ModuleInstances)
    
    for module in ModuleDefs.keys():
        ast = ModuleDefs[module]
        MODINSTS = {}
        analyze_AST(ast, MODINSTS)

        COORDINATES = get_coordinates(cloc, \
                                      phase, \
                                      design)
        '''
        if phase != 'synth':
            DELAYS = get_delays(cloc, \
                               design)
        else:
            DELAYS = {}
        '''
        DELAYS = get_delays(cloc, design)
    
        LABELS = get_labels(cloc, \
                           design)
    
        ERRNETMAP = get_errnetmap(lloc, \
                                design)
        
        slack_factor = get_slack(cloc, \
                                 design)
        
        crit_nodes = get_critical_node(cloc, \
                                       design)
    
        modules = list(set([MODINSTS[mod][0] for mod in MODINSTS.keys()]))
        module_def_needed = list((set(modules).difference(set(lxcvu440.keys())))) 
        
        # NOTE: Uncomment the following three lines later
        #if module_def_needed:
            #print('Please add the port definition of the following primitive(s) in logic.py: ' \
            #        + ', '.join(module_def_needed))
            #exit(0)

        ngraph, var_def_chain, var_use_chain, net_fo_map, wire_coord_map, Primitives = process(MODINSTS, \
                                                                   COORDINATES, \
                                                                   gnd, \
                                                                   vcc
                                                                   )
        '''
        overlap_bbox_area_dict, overlap_bbox_area_true, bbox_area_dict = construct_bboxes(wire_coord_map)
        congestion_factor = estimate_congestion_per_net(overlap_bbox_area_dict, \
                overlap_bbox_area_true, bbox_area_dict, net_fo_map)
        '''
        congestion_factor = {}


        node_dict, edge_dict = complete_graph(ngraph, var_def_chain, var_use_chain, DELAYS, LABELS, \
                                              ERRNETMAP, congestion_factor, slack_factor, crit_nodes)


        # Subgraph extraction for Critical net stuff
        lloc_ = lloc + '/' + design
        mkdir(lloc_)

        gloc_ = gloc + '/' + design
        mkdir(gloc_)

        # Adder-chain mapping extraction
        if phase == 'synth':
            root_path = os.path.join(gloc[:gloc.rfind('/')], 'Vivado_HLS.100')
            def_op_use, add_op_dict, hls_dep_gs, hls_add_feature_estimation, hls_clusters, edgelabeldict, coi_dict_add, ograph = exhg(root_path, design, gloc_)
            acex(ngraph, Primitives, design, module, phase, gloc_, add_op_dict, def_op_use, \
                    hls_dep_gs, hls_add_feature_estimation, hls_clusters, edgelabeldict, coi_dict_add, ograph)
    
    exit(0)

    if phase == 'place':
        subgraphs, subgraph_fused, delay_dict = exsg(ngraph, MODINSTS, design, phase, wire_coord_map, \
                gloc_, lloc_)

    # NOTE: Congestion is tricky. It is a 10 element long list where the first 5 elements comes from the fan-in
    #       edges and the last 5 elements come from fan-out edges
    node_content = printTable3(node_dict, ['Instance Name', 'Primitive Name', 'Coordinates', \
                                           'Fan In', 'Fan Out', 'Predecessor Node', 'Successor Node', \
                                           'Cell Delay', 'Congestion', 'Slack', 'Label'])
    nodefile = open(lloc_ + '/' + design + '_' + phase + '.nodelist', 'w')
    nodefile.write(node_content)
    nodefile.close()

    edge_content = printTable3(edge_dict, ['Edge Num', 'Source Primitive', ' Dest Primitive', \
                                           'Wire Name', 'Delay Place', 'Delay Route', 'Delay Stretch', \
                                           'Congestion Factor', 'Slack Value'\
                                           'Weight', 'Source Pin', 'Destination Pin', 'Label'])
    edgefile = open(lloc_ + '/' + design + '_' + phase + '.edgelist', 'w')
    edgefile.write(edge_content)
    edgefile.close()
   
    if phase == 'place':
        if not DELAYS['place'] or not DELAYS['route']:
            delay_content = printTable3(delay_dict, ['Edge Num', 'Wire Name', 'Destination PIN'])
            delayfile = open(lloc_ + '/' + design + '_' + phase + '.delaylist', 'w')
            delayfile.write(delay_content)
            delayfile.close()
    
    pg(ngraph, gloc, design, phase, Primitives)

    return

def generate_code(ast):
    codegen = ASTCodeGenerator()
    code = codegen.visit(ast)
    return code

def complete_graph(ngraph, var_def_chain, var_use_chain, DELAYS, LABELS, ERRNETMAP, \
        congestion_factor, slack_factor, crit_nodes):
    
    edge_dict = {}
    edge_num = 0
    node_dict = {}
    # NOTE: This is for two-parse delay result. Apppend later 
    # delay_dict = {}

    errnets = ERRNETMAP.keys()

    def_vars = var_def_chain.keys()
    for def_var in def_vars:
        dinstances = var_def_chain[def_var]
        for dinstance in dinstances:
            try:
                uinstances = var_use_chain[def_var]
            except KeyError:
                #print('>' * 3 + ' ' + def_var + ' is a primary output')
                continue
            for uinstance in uinstances:
                if not ngraph.has_edge(dinstance[0], uinstance[0]) or \
                        ngraph.edges[dinstance[0], uinstance[0]]['wire'] != def_var:
                    if not DELAYS['place']:
                        delay_place = 0.0
                    else:
                        try:
                            def_var_ = ERRNETMAP[def_var] if def_var in errnets else def_var
                            delay_place = DELAYS['place'][tuple([def_var_.replace(' ', '').rstrip(), \
                                                          uinstance[0] + \
                                                          '/' + uinstance[1]])]
                            #if not def_var_ == def_var:
                            #    print(def_var_)
                        # NOTE: This except is to handle the delay values that are not on timing paths
                        # NOTE: i.e., nets that are on the paths between non-sequential elements or 
                        # NOTE: between primitives that are not REGISTERS
                        except KeyError:
                            delay_place = 0.0
                            
                    if not DELAYS['route']:
                        delay_route = 0.0
                    else:
                        try:
                            def_var_ = ERRNETMAP[def_var] if def_var in errnets else def_var
                            delay_route = DELAYS['route'][tuple([def_var_.replace(' ', '').rstrip(), \
                                                          uinstance[0] + \
                                                          '/' + uinstance[1]])]
                        except KeyError:
                            delay_route = 0.0

                    if not LABELS:
                        gtruth = -1
                    else:
                        try:
                            def_var_ = ERRNETMAP[def_var] if def_var in errnets else def_var
                            gtruth = LABELS[tuple([def_var_.replace(' ', '').rstrip(), \
                                            uinstance[0] + \
                                            '/' + uinstance[1]])]
                        except KeyError:
                            gtruth = -1

                    if not congestion_factor:
                        cfactor_ = 0.0

                    if not slack_factor:
                        sfactor_ = 0.0
                    else:
                        try:
                            sfactor_ = slack_factor[def_var_]
                        except KeyError:
                            sfactor_ = 0.0
                    
                    '''
                    delay_place = 0.0 if not DELAYS['place'] \
                                      else DELAYS['place'][tuple([def_var, \
                                      uinstance[0] + '/' + uinstance[1]])]
                    delay_route = 0.0 if not DELAYS['route'] \
                                      else DELAYS['route'][tuple([def_var, \
                                      uinstance[0] + '/' + uinstance[1]])]
                    '''
                    delay_stretch = delay_route - delay_place

                    ngraph.add_edge(dinstance[0], uinstance[0], \
                                    wire=def_var, \
                                    delayp=delay_place, \
                                    delayr=delay_route, \
                                    delays=delay_stretch, \
                                    cfactor=cfactor_, \
                                    sfactor=sfactor_, \
                                    # TODO: Ask Ecenur about this delay thing once more. Understood.
                                    weight=0, \
                                    src_pin=dinstance[1], \
                                    dest_pin=uinstance[1], \
                                    grtruth=gtruth, \
                                    label=def_var + '\n' + '<' + dinstance[1] + ', ' + \
                                    uinstance[1] + '>')
                    # NOTE: For edgelist Chenhui format
                    edge_dict[str(edge_num)] = [dinstance[0], \
                                               uinstance[0], \
                                               def_var, \
                                               delay_place, \
                                               delay_route, \
                                               delay_stretch, \
                                               cfactor_, \
                                               sfactor_, \
                                               0, 
                                               dinstance[1], \
                                               uinstance[1], \
                                               gtruth
                                               ]
                    
    #                delay_dict[str(edge_num)] = [def_var.replace(' ', '').rstrip(), \
    #                                             uinstance[0] + '/' + uinstance[1]
    #                                             ]

                    edge_num = edge_num + 1
                    
                    # Special attention for the CARRY8 module for the adder chain
                    #if ngraph.nodes[dinstance[0]]['cell_type'] == 'CARRY8':
                    
                    #if def_var == 'D[15]':
                    #    print(dinstance[0])
                    #    print(dinstance[1])
                    #    pp.pprint(ngraph.nodes[dinstance[0]]['outp'])

                    ngraph.nodes[dinstance[0]]['outp'][dinstance[1]][def_var].append(uinstance[0])

                    #if ngraph.nodes[uinstance[0]]['cell_type'] == 'CARRY8':
                        #print('>' * 10)
                        #print('uinstance[0]: ', uinstance[0])
                        #print('uinstance[1]: ', uinstance[1])
                        #print('def_var: ', def_var)
                        #print('dinstance[0]: ', dinstance[0])
                        #ngraph.nodes[uinstance[0]]
                        #ngraph.nodes[uinstance[0]]['inp']
                        #pp.pprint(ngraph.nodes[uinstance[0]]['inp'][uinstance[1]])
                        #pp.pprint(ngraph.nodes[uinstance[0]]['inp'][uinstance[1]][def_var])
                    ngraph.nodes[uinstance[0]]['inp'][uinstance[1]][def_var].append(dinstance[0])
                        #print('#' * 10)
                        #print('\n\n')



    # NOTE: Once the graph construction is complete, getting its successors and predecessors
    nodes = ngraph.nodes()
    for node in nodes:
        successors = [succ for succ in ngraph.successors(node)]
        predecessors = [pred for pred in ngraph.predecessors(node)]
        ngraph.nodes[node]['succ'] = ','.join(successors)
        ngraph.nodes[node]['pred'] = ','.join(predecessors)
        ngraph.nodes[node]['fanin'] = len(predecessors)
        ngraph.nodes[node]['fanout'] = len(successors)
        ngraph.nodes[node]['grtruth'] = 1 if node in crit_nodes else 0
        # NOTE: For nodelist Chenhui format
        node_dict[node] = [ngraph.nodes[node]['cell_type'], \
                           ngraph.nodes[node]['coordinates'], \
                           ngraph.nodes[node]['fanin'], \
                           ngraph.nodes[node]['fanout'], \
                           ngraph.nodes[node]['pred'], \
                           ngraph.nodes[node]['succ'], \
                           ngraph.nodes[node]['delay'], \
                           ngraph.nodes[node]['congestion'], \
                           ngraph.nodes[node]['slack'], \
                           ngraph.nodes[node]['grtruth']
                           ]

    # NOTE: Uncomment the following three lines later
    #print('\n')
    #print('Total number of nodes: ' + str(ngraph.number_of_nodes()))
    #print('Total number of edges: ' + str(ngraph.number_of_edges()))

    return node_dict, edge_dict

def process(MODINSTS, COORDINATES, gnd, vcc):

    ngraph = nx.DiGraph()
    var_def_chain = {}
    var_use_chain = {}
    net_fo_map = {}
    wire_coord_map = {}
    
    instances = MODINSTS.keys()
    
    # Primitive map. The key is the Primitive type and the
    # value of the key is a list of instances that are of that
    # primitive type.
    Primitives = {}


    for instance in instances:
        module = MODINSTS[instance][0]
        if module == gnd or module == vcc:
            continue
        portmap = MODINSTS[instance][1]
        if module not in lxcvu440.keys():
            #print('Ignoring blackbox. Graph is no longer connected. It could be a forest.')
            continue
        INPORTS = lxcvu440[module]['IN']
        OUTPORTS = lxcvu440[module]['OUT']
        try:
            coord = COORDINATES[instance]
        except KeyError:
            coord = tuple([0, 0])
        
        IN = {}
        OUT = {}
        for inp in INPORTS:
            #IN[inp] = {'use_var': [], \
            #           'dinstances': []}
            IN[inp] = {}
        for outp in OUTPORTS:
            #OUT[outp] = {'def_var': [], \
            #             'uinstances': []}
            OUT[outp] = {}

        ngraph.add_node(instance, \
                        shape='circle', \
                        fillcolor=fill_color(module), \
                        style='filled', \
                        cell_type=module, \
                        # TODO: Get the RPM coordinates
                        coordinates=coord, \
                        # TODO: Calculate the FANIN and FANOUT value. They are now placeholders.
                        fanin=0, \
                        fanout=0, \
                        # TODO: Calculate the predecessor and successor nodes. Use Networkx function
                        pred=[], \
                        succ=[], \
                        # TODO: Need to understand what delay this is
                        delay=0.0, \
                        congestion=[0] * 10, \
                        slack=[0] * 10, \
                        grtruth=0, \
                        inp=IN, \
                        outp=OUT, \
                        label=module + '\n' + instance)

        del IN
        del OUT

        OUT = get_var_def_chain(var_def_chain, OUTPORTS, instance, coord, portmap, wire_coord_map)
        IN = get_var_use_chain(var_use_chain, INPORTS, instance, coord, portmap, wire_coord_map)

        ngraph.nodes[instance]['inp'] = IN
        ngraph.nodes[instance]['outp'] = OUT
        #if module == 'FDRE':
        #    print(instance, OUT)
        del IN
        del OUT

        if module not in Primitives.keys():
            Primitives[module] = [instance]
        else:
            Primitives[module].append(instance)
    
    def_vars = var_def_chain.keys()
    for def_var in def_vars:
        if def_var not in net_fo_map.keys():
            net_fo_map[def_var] = 0

        try:
            fo = len(var_use_chain[def_var])
        except KeyError:
            fo = 0

        net_fo_map[def_var] = len(var_def_chain[def_var]) + fo

    return ngraph, var_def_chain, var_use_chain, net_fo_map, wire_coord_map, Primitives

def get_var_def_chain(var_def_chain, OUTPORTS, instance, coord, portmap, wire_coord_map):
    # Can we use this oport to annotate src PIN name?
    OUT = {}

    for oport in OUTPORTS:
        #OUT[oport] = {'def_var': [], \
        #              'uinstances': []}

        #if instance == 'buff0_reg[5]__0':
        #    print(oport)
        
        OUT[oport] = {}
        try:
            aname = portmap[oport]
        except KeyError:
            aname = ''

        #if instance == 'buff0_reg[5]__0':
        #    print(aname)

        if not aname:
            continue
        for aname_ in aname:
            if aname_ not in var_def_chain.keys():
                var_def_chain[aname_] = [tuple([instance, oport])]
                wire_coord_map[aname_] = [coord]
                OUT[oport].update({aname_: []})
            else:
                var_def_chain[aname_].append(tuple([instance, oport]))
                wire_coord_map[aname_].append(coord)

    return OUT

def get_var_use_chain(var_use_chain, INPORTS, instance, coord, portmap, wire_coord_map):
    # Can we use this iport to annotate dest PIN name?
    IN = {}

    for iport in INPORTS:
        #IN[iport] = {'use_var': [], \
        #             'dinstances': []}
        IN[iport] = {}
        try:
            aname = portmap[iport]
        except KeyError:
            aname = ''

        if not aname:
            continue
        for aname_ in aname:
            if aname_ not in var_use_chain.keys():
                var_use_chain[aname_] = [tuple([instance, iport])]
                wire_coord_map[aname_] = [coord]
            else:
                var_use_chain[aname_].append(tuple([instance, iport]))
                wire_coord_map[aname_].append(coord)
            IN[iport].update({aname_: []})

    return IN

def parse_verilog(vfilelist, include, define):

    ast, directives = parse(vfilelist, \
                            include, \
                            define)
    #ast.show()
    return ast, directives

def analyze_AST(ast, MODINSTS):
    if(ast.__class__.__name__ == 'Instance'):
        instance_ = ast.name
        instance = instance_.lstrip('\/')
        module = ast.module

        portmap = {}
        parammap = {}
        # To get the port mapping
        portlist = ast.portlist
        for port in portlist:
            pname = port.portname
            aname = []
            get_args(port.argname, aname)
            portmap[pname] = aname
        # To get the parameter mapping
        parameterlist = ast.parameterlist
        for param in parameterlist:
            pname = param.paramname
            aname = []
            get_args(param.argname, aname)
            parammap[pname] = aname

        MODINSTS[instance] = [module, portmap, parammap]

    for c in ast.children():
        analyze_AST(c, MODINSTS)

    return

def get_modules(ast, ModuleDefs, ModuleInstances):
    if (ast.__class__.__name__ == 'ModuleDef'):
        module = ast.name
        ModuleDefs[module] = ast
    elif (ast.__class__.__name__ == 'Instance'):
        module = ast.module
        inst_name = ast.name
        if module not in ModuleInstances.keys():
            ModuleInstances[module] = [inst_name]
        else:
            ModuleInstances[module].append(inst_name)

    for c in ast.children():
        get_modules(c, ModuleDefs, ModuleInstances)

    return

def get_args(ast, aname):
    if ast.__class__.__name__ == 'Identifier':
        try:
            name = str(ast.name)
            aname.append(name.lstrip('\/'))
            return aname
        except AttributeError:
            name = str(ast.var)
            aname.append(name.lstrip('\/'))
            return aname
    elif ast.__class__.__name__ == 'Pointer':
        name = str(generate_code(ast))
        aname.append(name.lstrip('\/'))
        return aname
    elif ast.__class__.__name__ == 'Partselect':
        var = str(ast.var)
        var = var.lstrip('\/')
        msb = ast.msb.value
        lsb = ast.lsb.value
        '''
        print(str(var), int(msb), int(lsb))
        '''
        for i in range(int(msb), int(lsb) - 1, -1):
            aname.append(str(var) + '[' + str(i) + ']')
        return aname
    else:
        for c in ast.children():
            get_args(c, aname)

def get_coordinates(cloc, phase, design):
    
    COORDINATES = {}
    if phase == 'synth':
        return COORDINATES

    cloc_ = cloc + '/' + design

    cfile = open(cloc_ + '/' + design + '_verilog_netlist_' + phase + '.loc', 'r')
    clines = cfile.readlines()
    for line in clines:
        c = line.split()
        # NOTE: Coordinates in the format of name of instance and the tuple (X_Coord, Y_Coord)
        COORDINATES[c[0]] = tuple([int(c[1]), int(c[2])])

    visualize(cloc_, design, phase)

    return COORDINATES

def get_delays(cloc, design):

    DELAYS = {}
    phases = ['route', 'place', 'synth']

    for phase in phases:
        DELAYS[phase] = {}
        try:
            dfile = open(cloc + '/' + design + '/' + design + \
                    '_' + phase + '.delay', 'r')
        except FileNotFoundError:
            #return DELAYS
            continue
        dlines = dfile.readlines()
        dfile.close()
        for dline in dlines:
            d = dline.split()
            max_net_delay = max([int(i) for i in d[2:]])
            key = tuple([d[0], d[1]])
            #DELAYS[d[0]] = {d[1]: d[2]}
            DELAYS[phase][key] = max_net_delay

    return DELAYS

def get_slack(cloc, design):

    slack_factor = {}
    try:
        sfile = open(cloc + '/' + design  + '/' + design + \
                '_netinfo.txt', 'r')
    except FileNotFoundError:
        return slack_factor
    
    slines = sfile.readlines()
    sfile.close()

    for sline in slines:
        s = sline.split()
        slack_factor[s[0]] = float(s[1])

    return slack_factor
    
def get_critical_node(cloc, design):
        
    try:
        cfile = open(cloc + '/' + design + '/' + design + \
                '_criticalnodes.txt', 'r')
    except FileNotFoundError:
        return []

    clines_ = cfile.readlines()
    cfile.close()

    clines = [cline[:cline.rfind('/')] for cline in clines_]

    return clines

def get_labels(cloc, design):
    
    LABELS = {}

    try:
        lfile = open(cloc + '/' + design + '/' + design + '_gtruth.labels', 'r')
    except FileNotFoundError:
        return LABELS

    llines = lfile.readlines()
    lfile.close()
    
    for lline in llines:
        l = lline.split()
        key = tuple([l[0], l[1]])
        LABELS[key] = l[2]

    return LABELS

def get_errnetmap(lloc, design):

    ERRNETMAP = {}
    try:
        efile = open(lloc + '/' + design + '/' + design + '_place.errnetmap', 'r')
    except FileNotFoundError:
        return ERRNETMAP

    elines = efile.readlines()
    efile.close()
    
    for eline in elines:
        l = eline.split()
        ERRNETMAP[l[1]] = l[2]

    return ERRNETMAP

'''
def printTable2(myDict, colList=None):
    #if not colList:
    #    colList = list(myDict[0].keys() if myDict else [])
    #myList = [colList]
    myList = []

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    #formatStr = '\t '.join(["{{:^{}}}".format(i) for i in colSize])
    formatStr = ' '.join(["{{:^{}}}".format(i) for i in range(len(colSize))])
    #myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content
'''
