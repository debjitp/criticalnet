#===============================================================================
#
#         FILE: top.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 14-01-2020
#     REVISION: 
#    LMODIFIED: Thu 30 Jan 2020 05:06:16 PM EST
#===============================================================================
'''
python top.py -n /home/dp638/Work-22/CriticalNet/netlist -g /home/dp638/Work-22/CriticalNet/graph -d harris_1 -l /home/dp638/Work-22/CriticalNet/graph_data -c /home/dp638/Work-22/CriticalNet/coord -p place -r GND -v VCC
'''

import os, sys
from argparse import ArgumentParser
from netlist_to_graph import main


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-n', '--netlist', help="Netlist location with absolute path", \
            dest='nloc', required=True)
    parser.add_argument('-g', '--grpah', help="Graph location with absolute path", \
            dest='gloc', required=True)
    parser.add_argument('-l', '--log', help="Graph log location with absolute path", \
            dest='lloc', required=True)
    parser.add_argument('-c', '--coord', help="Cell Coordinate File location with absolute path", \
            dest='cloc', required=True)
    parser.add_argument('-r', '--ground', help="Name of Ground PIN", \
            dest='ground', required=True)
    parser.add_argument('-v', '--vcc', help="Name of VCC PIN", \
            dest='vcc', required=True)
    parser.add_argument('-p', '--phase', help="Phase of VIVADO flow", \
            dest='phase')
    parser.add_argument('-d', '--design', help="Name of the design", \
            dest='design', required=True)
    options = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        exit(0)

    if not options.phase:
        phases = ['place', 'route']
    else:
        phases = [options.phase]

    for phase in phases:
        files = options.nloc + '/' + options.design + '/' + options.design + '_verilog_netlist_' + phase + '.v'
        if not os.path.exists(files):
            continue
        main(files, \
             options.gloc, \
             options.lloc, \
             options.cloc, \
             options.ground, \
             options.vcc, \
             phase, \
             options.design)
