#!/bin/bash

# Pass a command line argument of 0 if netlists for both placement and routing needs to be considered and 
# delay values are not available
# Pass a command line argument of 1 if edgelists need to be created with delay and stretching annotation 
# for graph learning

if [ -z $1 ]
then
    echo -e "Please specify which graph(s) you wish to construct. 0 for both place/route and 1 for place"
    exit 1
fi

if [ -z $2 ]
then
    echo -e "Please specify the location where VIVADO run directory will be made"
    exit 1
fi

WORKDIR=$2
PYTHONSRCDIR=$WORKDIR/src

#benchmarks='harris_5 harris_6 sobel_1 gaussblur_1'
benchmarks='harris_1 harris_5 harris_6 sobel_1 gaussblur_1 paintmask_1'

cd $PYTHONSRCDIR

for benchmark in $benchmarks
do
    if [ $1 -eq 0 ]
    then
        echo -e "Working on (both placed and routed netlists): "$benchmark
        python top.py -n /home/dp638/Work-22/CriticalNet/netlist -g /home/dp638/Work-22/CriticalNet/graph -d $benchmark -l /home/dp638/Work-22/CriticalNet/graph_data -c /home/dp638/Work-22/CriticalNet/coord -r GND -v VCC
    elif [ $1 -eq 1 ]
    then
        echo -e "Working on (only placed netlist): "$benchmark
        python top.py -n /home/dp638/Work-22/CriticalNet/netlist -g /home/dp638/Work-22/CriticalNet/graph -d $benchmark -l /home/dp638/Work-22/CriticalNet/graph_data -c /home/dp638/Work-22/CriticalNet/coord -r GND -v VCC -p place
    fi
done

cd -
