#!/bin/bash

# Setting up the working directories
WORKDIR=$1

export TCLDIR=$WORKDIR/tcl
export XDCDIR=$WORKDIR/xdc
COORD=$WORKDIR/coord
RUNDIR=$WORKDIR/vivado_runs

VHDLSRCDIR=$WORKDIR/vhdl
NETLIST=$WORKDIR/netlist

# Declaring an array for the design name as the key and the top module name as the value
declare -A des_name_mod_name

des_name_mod_name["houghlines2_1"]="my_image_filter"
#des_name_mod_name+=( ["harris_1"]="harris_1" )
#des_name_mod_name+=( ["harris_5"]="harris_5" )
#des_name_mod_name+=( ["harris_6"]="harris_6" )
#des_name_mod_name+=( ["diffeq2"]="diffeq2" )

## Running all benchmarks
for key in ${!des_name_mod_name[@]}
do
    top_module=${des_name_mod_name[${key}]}
    benchmark=$key
    echo -e "\n\n"
    echo -e "Working on: "$benchmark" Module: "$top_module
    export bmark=$benchmark
    export top_module
    export NETLISTDIR=$NETLIST/$benchmark
    export VHDLDIR=$VHDLSRCDIR/$benchmark
    export COORDDIR=$COORD/$benchmark
    
    if [ ! -d $RUNDIR/$benchmark ]
    then
        echo -e "Making directory: "$RUNDIR/$benchmark
        mkdir -pv $RUNDIR/$benchmark
    fi

    if [ ! -d $NETLISTDIR ]
    then
        echo -e "Making directory: "$NETLISTDIR
        mkdir -pv $NETLISTDIR
    fi

    if [ ! -d $COORDDIR ]
    then
        echo -e "Making directory: "$COORDDIR
        mkdir -pv $COORDDIR
    fi

    cd $RUNDIR/$benchmark
    vivado -mode batch -source $TCLDIR/vhdl.tcl

    cd -

    unset bmark
    unset top_module
    unset NETLISTDIR
    unset VHDLDIR
    unset COORDDIR
done
