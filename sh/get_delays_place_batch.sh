#!/bin/bash

# Setting up the working directories
WORKDIR=$1
TCLDIR=$WORKDIR/tcl
export VIVADODIR=$WORKDIR/vivado_runs

GRAPH_DATA=$WORKDIR/graph_data
COORD=$WORKDIR/coord

# List of benchmarks to be run
benchmarks='harris_1 harris_5 harris_6 sobel_1 gaussblur_1 paintmask_1'
phases='place'

## Running all benchmarks
for benchmark in $benchmarks
do
    echo -e "\n\n"
    export bmark=$benchmark
    export NETDIR=$GRAPH_DATA/$benchmark
    export DELAYDIR=$COORD/$benchmark

    for phse in $phases
    do
        export phase=$phse
        echo -e "Working on: "$benchmark" "$phase
    
        cd $VIVADODIR/$benchmark
        vivado -mode batch -source $TCLDIR/timing_${phse}.tcl

        cd -
        unset phase
    done
    unset bmark
    unset NEDIR
    unset DELAYDIR
done
