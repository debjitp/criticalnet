# Get all setup varibales from bash enviroment
set design_name $env(bmark)
set phase $env(phase)
set net_dir $env(NETDIR)
set delay_dir $env(DELAYDIR)
set vivado_dir $env(VIVADODIR)

set delay_src_file_name [file join $net_dir ${design_name}_place.delaylist]
set delay_err_file_name [file join $net_dir ${design_name}_${phase}.delayerrlist]
set delay_dest_file_name [file join $delay_dir ${design_name}_${phase}.delay]
set checkpoint_file_name [file join $vivado_dir $design_name post_${phase}.dcp]

open_checkpoint $checkpoint_file_name

# Get the delay values for a pair of wire and destination_cell/PIN
set delay_dest_file [open $delay_dest_file_name w]
set delay_err_file [open $delay_err_file_name w]
set delay_src_file [open $delay_src_file_name r]

set content [read $delay_src_file]
close $delay_src_file

set lines [split $content "\n"]
foreach line $lines {
    if {$line eq ""} {
        continue
    }
    set line_data [split $line " "]
    set net_name [lindex $line_data 1]
    set pin_name [lindex $line_data 2]
    
    # The following if-else block is for error catching
    # in case any net name is wrong. It will help to continue
    # the rest of the iterative loop. We dump the name of the disturbing
    # net names in the dela_err_file.
    set net_exist [get_nets $net_name]
    if {$net_exist == {}} {
        puts $delay_err_file "$line"
    } else {
        set delay [get_property SLOW_MAX [get_net_delays -of [get_nets $net_name] -to [get_pins $pin_name]]]
        puts $delay_dest_file "$net_name $pin_name $delay"
    }
}

close $delay_dest_file
close $delay_err_file
