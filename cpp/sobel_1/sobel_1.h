#ifndef MY_SOBEL_H
#define MY_SOBEL_H

#define MAX_WIDTH 220
#define MAX_HEIGHT 220

/* Source: https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841665/HLS+Video+Library */
#include "hls_video.h"
#include <ap_fixed.h>

typedef hls::stream<ap_axiu<32, 1, 1, 1> > AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3> MY_IMAGE;
typedef hls::Scalar<3, unsigned char> MY_PIXEL;

void my_image_filter(
        AXI_STREAM& INPUT_STREAM,
        AXI_STREAM& OUTPUT_STREAM, 
        int rows,
        int cols
        );

#endif
