#include "sobel_1.h"

void my_image_filter(
        AXI_STREAM& INPUT_STREAM, 
        AXI_STREAM& OUTPUT_STREAM, 
        int rows, 
        int cols
        ) {

#pragma HLS INTERFACE axis port=INPUT_STREAM
#pragma HLS INTERFACE axis port=OUTPUT_STREAM
#pragma HLS dataflow


MY_IMAGE img_0(rows, cols); // Source image
MY_IMAGE img_1(rows, cols); // Destination image
MY_IMAGE img_2(rows, cols);
MY_IMAGE img_3(rows, cols);
MY_IMAGE img_4(rows, cols);
MY_IMAGE img_5(rows, cols);
/* https://www.rapidtables.com/web/color/gray-color.html*/
MY_PIXEL scalar_pix(128, 128, 128);

/* Convert AXI4 stream to hls::Mat format as defined in the header file */
hls::AXIvideo2Mat(INPUT_STREAM, img_0);

/* Apply Sobel filter on the image */
hls::Sobel<1, 0, 5>(img_0, img_1);

/* Do fun operations to increase the code size */
hls::SubS(img_1, scalar_pix, img_2); // Without mask (https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841696/HLS+SubS)
hls::Scale(img_2, img_3, 2, 0);
hls::Erode(img_3, img_4);
hls::Dilate(img_4, img_5);

/* Fun done. Convert back the image to AXI Stream */
hls::Mat2AXIvideo(img_5, OUTPUT_STREAM);

}

int main (int argc, char **argv) {
    AXI_STREAM src_axi, dest_axi;
    const int rows = MAX_HEIGHT;
    const int cols = MAX_WIDTH;

    my_image_filter(src_axi, dest_axi, rows, cols);

    return 0;
}



