#include "hls_video.h"
 
typedef hls::Mat<720,1280,HLS_8U> GS_IMAGE;

 

//void tsd_harriscorners(GS_IMAGE &src, GS_IMAGE &dst, int k){
void harris_1(GS_IMAGE &src, GS_IMAGE &dst, int k){

 

hls::CornerHarris<3,3>(src,dst,k);

 

}
