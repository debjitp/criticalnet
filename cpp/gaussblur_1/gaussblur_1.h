#ifndef MY_GBLUR_H
#define MY_GBLUR_H

#define MAX_WIDTH 256
#define MAX_HEIGHT 256

#include "hls_video.h"
#include <ap_fixed.h>
#include <ap_int.h>

typedef hls::stream<ap_axiu<32, 1, 1, 1> > AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3> MY_IMAGE;
typedef hls::Scalar<3, unsigned char> MY_PIXEL;

typedef ap_fixed<18, 6, AP_RND> MY_COEFF;
typedef ap_int<32> MY_INDEX;

const int KSize = 5;

void my_image_filter(
        AXI_STREAM& INPUT_STREAM, 
        AXI_STREAM& OUTPUT_STREAM, 
        int rows, 
        int cols
        );

template<typename IMAGE_TYPE, typename PIXEL_TYPE>
void replicate_to_three_channels(
        IMAGE_TYPE& IMAGE_IN, 
        IMAGE_TYPE& IMAGE_OUT_0,
        IMAGE_TYPE& IMAGE_OUT_1,
        IMAGE_TYPE& IMAGE_OUT_2,
        int rows, 
        int cols
        );

template<typename IMAGE_TYPE>
void apply_filter(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT,
        int rows, 
        int cols
        );

template<typename IMAGE_TYPE>
void apply_blur(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT
        );

template<typename IMAGE_TYPE>
void apply_fun_effect(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT,
        int rows,
        int cols
        );

template<typename IMAGE_TYPE, typename PIXEL_TYPE>
void combine_images(
        IMAGE_TYPE& IMAGE_IN_0,
        IMAGE_TYPE& IMAGE_IN_1,
        IMAGE_TYPE& IMAGE_IN_2,
        IMAGE_TYPE& IMAGE_OUT, 
        int rows, 
        int cols
        );

#endif
