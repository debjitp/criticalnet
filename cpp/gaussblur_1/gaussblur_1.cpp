#include "gaussblur_1.h"

void my_image_filter(
        AXI_STREAM& INPUT_STREAM, 
        AXI_STREAM& OUTPUT_STREAM,
        int rows,
        int cols
        ) {
#pragma HLS INTERFACE axis port=INPUT_STREAM
#pragma HLS INTERFACE axis port=OUTPUT_STREAM
#pragma HLS dataflow

    MY_IMAGE img_0(rows, cols);
    MY_IMAGE img_1(rows, cols);
    MY_IMAGE img_2(rows, cols);
    MY_IMAGE img_3(rows, cols);
    MY_IMAGE img_4(rows, cols);
    MY_IMAGE img_5(rows, cols);
    MY_IMAGE img_6(rows, cols);
    MY_IMAGE img_7(rows, cols);

    MY_PIXEL scalar_pix(128, 128, 128);

    /* Convert AXI4 stream to hls::Mat format as defined in the header file */
    hls::AXIvideo2Mat(INPUT_STREAM, img_0);

    /* Replicate the input image to three different channels to do fun operations */
    replicate_to_three_channels<MY_IMAGE, MY_PIXEL>(img_0, img_1, img_2, img_3, rows, cols);

    /* Fun operation: 1 */
    apply_filter<MY_IMAGE>(img_1, img_4, rows, cols);
    /* Fun operation: 2 */
    apply_blur<MY_IMAGE>(img_2, img_5);
    /* Fun operation: 3 */
    apply_fun_effect<MY_IMAGE>(img_3, img_6, rows, cols);
    /* Combine fun operations */
    combine_images<MY_IMAGE, MY_PIXEL>(img_4, img_5, img_6, img_7, rows, cols);
    
    /* Fun done. Convert back the image to AXI stream */
    hls::Mat2AXIvideo(img_7, OUTPUT_STREAM);

}

template<typename IMAGE_TYPE, typename PIXEL_TYPE>
void replicate_to_three_channels(
        IMAGE_TYPE& IMAGE_IN, 
        IMAGE_TYPE& IMAGE_OUT_0,
        IMAGE_TYPE& IMAGE_OUT_1,
        IMAGE_TYPE& IMAGE_OUT_2,
        int rows, 
        int cols
        ) {
    
    PIXEL_TYPE channel_in;
    PIXEL_TYPE channel_out;

    for(int row = 0; row < rows; row++) {
        for(int col = 0; col < cols; col++) {
            IMAGE_IN >> channel_in;
            channel_out = channel_in;
            IMAGE_OUT_0 << channel_out;
            IMAGE_OUT_1 << channel_out;
            IMAGE_OUT_2 << channel_out;

        }
    }
}

template<typename IMAGE_TYPE>
void apply_filter(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT,
        int rows, 
        int cols
        ) {
    const MY_COEFF coeff_vert[KSize][KSize] = {
        {-7, -4, 2, -6, -8},
        {2, 7, -7, -7, 4},
        {-10, 1, 2, 5, -6},
        {-1, -7, 9, 6, 7},
        {-7, 8, 4, 7, -3},
    };
    /* Creating the Window for applying 2d filtering */
    hls::Window<KSize, KSize, MY_COEFF> KVertical;
    for(int i = 0; i < KSize; i++) {
        for(int j = 0; j < KSize; j++) {
            KVertical.val[i][j] = coeff_vert[i][j];
        }
    }
    /* Creating the anchor point for 2d filtering  */
    hls::Point_<MY_INDEX> Anchor;
    Anchor.x = -1;
    Anchor.y = -1;

    hls::Filter2D(IMAGE_IN, IMAGE_OUT, KVertical, Anchor);
}

template<typename IMAGE_TYPE>
void apply_blur(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT
        ) {

    hls::GaussianBlur<5, 5>(IMAGE_IN, IMAGE_OUT);
}

template<typename IMAGE_TYPE>
void apply_fun_effect(
        IMAGE_TYPE& IMAGE_IN,
        IMAGE_TYPE& IMAGE_OUT,
        int rows,
        int cols
        ) {

    IMAGE_TYPE IMAGE_TEMP_1(rows, cols);
    IMAGE_TYPE IMAGE_TEMP_2(rows, cols);

#pragma HLS dataflow
    hls::Scale(IMAGE_IN, IMAGE_TEMP_1, 2, 0);
    hls::Erode(IMAGE_TEMP_1, IMAGE_TEMP_2);
    hls::Dilate(IMAGE_TEMP_2, IMAGE_OUT);
}

template<typename IMAGE_TYPE, typename PIXEL_TYPE>
void combine_images(
        IMAGE_TYPE& IMAGE_IN_0,
        IMAGE_TYPE& IMAGE_IN_1,
        IMAGE_TYPE& IMAGE_IN_2,
        IMAGE_TYPE& IMAGE_OUT, 
        int rows, 
        int cols
        ) {
    PIXEL_TYPE channel_in;
    PIXEL_TYPE channel_out;

    for(int row = 0; row < rows; row++) {
        for(int col = 0; col < cols; col++) {
            if (row < rows / 2) {
                if (col < cols / 2) {
                    IMAGE_IN_0 >> channel_in;
                } else {
                    IMAGE_IN_1 >> channel_in;
                }
            } else {
                if (col < cols / 2) {
                    IMAGE_IN_2 >> channel_in;
                } else {
                    IMAGE_IN_0 >> channel_in;
                }
            }
            channel_out = channel_in;
        }
    }
}


int main(int argc, char **argv) {
    AXI_STREAM src_axi, dest_axi;
    const int rows = MAX_HEIGHT;
    const int cols = MAX_WIDTH;

    my_image_filter(src_axi, dest_axi, rows, cols);

    return 0;
}
