#ifndef MY_PMASK_H
#define MY_PMASK_H

#define MAX_WIDTH 256
#define MAX_HEIGHT 256

#include "hls_video.h"
#include <ap_fixed.h>

typedef hls::stream<ap_axiu<32, 1, 1, 1> > AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3> MY_RGB_IMAGE;
typedef hls::Scalar<3, unsigned char> MY_PIXEL;

typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1> MY_GRAY_IMAGE;

void my_image_filter(
        AXI_STREAM& INPUT_STREAM, 
        AXI_STREAM& OUTPUT_STREAM, 
        int rows, 
        int cols
        );

#endif
