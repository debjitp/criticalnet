#include "paintmask_1.h"

void my_image_filter(
        AXI_STREAM& INPUT_STREAM, 
        AXI_STREAM& OUTPUT_STREAM,
        int rows,
        int cols
        ) {
#pragma HLS interface axis port=INPUT_STREAM
#pragma HLS interface axis port=OUTPUT_STREAM
#pragma HLS dataflow

    MY_RGB_IMAGE init_image(rows, cols);
    MY_RGB_IMAGE final_image(rows, cols);

    MY_RGB_IMAGE img_1(rows, cols);
    MY_RGB_IMAGE img_2(rows, cols);
    MY_GRAY_IMAGE img_3(rows, cols);
    MY_GRAY_IMAGE img_4(rows, cols);
    MY_GRAY_IMAGE img_5(rows, cols);

    MY_RGB_IMAGE img_6(rows, cols);

    hls::AXIvideo2Mat(INPUT_STREAM, init_image);
    /* Duplicate to use PaintMask*/
    hls::Duplicate(init_image, img_1, img_2);
    /* NOTE: Converting a RGB image to GRAY scale image 
     *       The reason for doing this is that Harris has a peculiar assertion inside. 
     *       assert(SRC_T == HLS_8UC1 || SRC_T == HLS_32FC1)
     *       which means it only accepts 8-bit unsigned char or 32-bit float images. 
     * */
    hls::CvtColor<HLS_RGB2GRAY>(img_1, img_3);
    /* Noise reduction */
    hls::GaussianBlur<5, 5>(img_3, img_4);
    /* Apply Harris edge/corner detetctor. Harris was not working after csynth with
     * black box module remaining. Hence using CornerHarris
     */
    hls::CornerHarris<5, 3, int>(img_4, img_5, 1);
    /* Convert back to RGB */
    hls::CvtColor<HLS_GRAY2RGB>(img_5, img_6);

    hls::Scalar<3, unsigned char> color(0, 0, 255);
    hls::PaintMask(img_2, img_6, final_image, color);

    hls::Mat2AXIvideo(final_image, OUTPUT_STREAM);
}

/*
template <unsigned int max_lines>
void my_hough_plotting(
        MY_GRAY_IMAGE& INPUT_IMAGE, 
        MY_GRAY_IMAGE& OUTPUT_IMAGE, 
        hls::Polar_< float, float > (&lines)[max_lines]
        ) {
    hls::Scalar<1, unsigned char> PixelS;
    hls::Scalar<1, unsigned char> PixelH;

    HLS_SIZE_T rows = INPUT_IMAGE.rows;
    HLS_SIZE_T cols = INPUT_IMAGE.cols;
    
    float s, c, d, y0;
    int Point;

    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            INPUT_IMAGE >> PixelS;
            Point = 0;
                for (int max_ = 0; max_ < max_lines; max_++) {
                    // Follwoing conversion of rho = col * cos(theta) + row * sin(theta) copied from a 
                    // Xilinx forum post
                    cordic_apfixed::sinh_cosh_range_redux_cordic<1, 1>(lines[max_].angle, s, c);
                    y0 = (lines[max_].rho - col * c) / s;
                    d = row - y0;
                    if ((d > -20) && (d <= 2)) {
                        Point = 1;
                    }
                    if (Point == 1) {
                        PixelH.val[0] = 255;
                    } else {
                        PixelH.val[0] = 0;
                    }
                }
            OUTPUT_IMAGE << PixelH;
        }
    }
}
*/

int main(int argc, char **argv) {
    AXI_STREAM src_axi, dest_axi;
    const int rows = MAX_HEIGHT;
    const int cols = MAX_WIDTH;

    my_image_filter(src_axi, dest_axi, rows, cols);

    return 0;
}
